#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "Function/RooBVNPdf.cxx"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooDecay.h"
#include "RooGaussModel.h"
#include "RooCBShape.h"
#include "RooPolynomial.h"
#include "RooExponential.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooFitResult.h"
#include "RooAddModel.h"
#include "RooExtendPdf.h"
#include "RooChebychev.h"
#include "RooDataHist.h"
#include "RooArgusBG.h"
#include "RooGenericPdf.h"
#include "RooMinimizer.h"
#include "TCanvas.h"
#include "TText.h"
#include "RooPlot.h"
#include <iostream>
#include <string>
#include "myCharmFitter_2D.h"

myCharmFitter::myCharmFitter(){
	m_debug = false;
	m_is_weighted = true;
	m_extraPlots = false;
	m_fix_decay = false;
	useInput = 1;
	drawOnly = 0;

	m_ReducedChi2 = 0.;
	nparams = 0;

	m_mframe = 0;
	t_mframe = 0;
	m_mframe_RangeNP = 0;
	m_mframe_RangeP = 0;
	m_mframe_RangeMisID = 0;
	t_mframe_RangeJpsi = 0;
	t_mframe_RangePsi = 0;

	tau     = new RooRealVar("tau", "Pseudoproper time", -1.0,15.,"ps");
	//m       = new RooRealVar("m", "Invariant mass", 2.6,3.5,"GeV");
	m       = new RooRealVar("m", "Invariant mass", 2.6,4.2,"GeV");
	//m       = new RooRealVar("m", "Invariant mass", 2.9,3.25,"GeV");
	//tau_err = new RooRealVar("tau_err", "Pseudoproper time error", 0, 0.5, "ps");
	//m_err   = new RooRealVar("m_err", "Invariant mass error", 0., 0.2, "GeV");

	RooResolutionModel* mPDF_Tau_Reso;

	mPDF_Tau_NP_1S = 0;
	mPDF_Tau_NP_2S = 0;
	mPDF_Tau_NP_B1 = 0;
	mPDF_Tau_NP_B2 = 0;

	mPDF_Mass_Signal_1S = 0;
	mPDF_Mass_Signal_2S = 0;
	mPDF_Mass_Bkgd_Prompt = 0;
	mPDF_Mass_Bkgd_NonPrompt = 0;
	mPDF_Mass_Bkgd_MisID = 0;
}

RooFitResult* myCharmFitter::Nominal_Fit(bool badFit, RooDataSet* data, RooArgSet* savedParams, TString* inputF, TString* outF, int iy, double pt, bool angleOn, int sys){
	//RooAbsPdf::defaultIntegratorConfig()->setEpsRel(1e-8) ;
	//RooAbsPdf::defaultIntegratorConfig()->setEpsAbs(1e-8) ;

	////////////////////////////////////////////////////////
	// Time Signal
	////////////////////////////////////////////////////////
	RooRealVar    Tau_Mean("Tau_Mean","tau mean",0.0, -0.1,0.1);
	Tau_Mean.setConstant(kTRUE);
	RooRealVar    Tau_Mean1("Tau_Mean1","tau mean 1",0.0, -0.01,0.01);
	RooRealVar    Tau_Mean2("Tau_Mean2","tau mean 2",0.0, -0.01,0.01);
	RooRealVar    Tau_Mean3("Tau_Mean3","tau mean 2",0.0, -0.015,0.015);
	//RooRealVar    Tau_Sigma1("Tau_Sigma1", "Gaussian reso sigma", 0.016, 0.005, 0.075);//0.06
	RooRealVar    Tau_Sigma1("Tau_Sigma1", "Gaussian reso sigma", 0.02, 0.01, 0.1);//0.06
	//RooRealVar    Tau_SigmaScal("Tau_SigmaScale","scaling between double Gaussian resolution",1.59,1.1,2.5);//1.83
	RooRealVar    Tau_SigmaScale("Tau_SigmaScale","scaling between double Gaussian resolution",2,1.5,4);//1.83
	RooFormulaVar Tau_Sigma2("Tau_Sigma2", "@0 * @1",RooArgList(Tau_Sigma1,Tau_SigmaScale));
	//RooRealVar    Tau_SigmaScal1("Tau_SigmaScale1","scaling between double Gaussian resolution",2.7,2.7,3.5);//2.4
	RooRealVar    Tau_SigmaScale1("Tau_SigmaScale1","scaling between double Gaussian resolution",3,3,7);//2.4
	RooFormulaVar Tau_Sigma3("Tau_Sigma3", "@0 * @1",RooArgList(Tau_Sigma1,Tau_SigmaScale1));
	//RooRealVar    Tau_SigmaScal2("Tau_SigmaScale2","scaling between double Gaussian resolution",3.9,3.9,6.0);//4.0
	RooRealVar    Tau_SigmaScale2("Tau_SigmaScale2","scaling between double Gaussian resolution",5,4,6);//4.0
	RooFormulaVar Tau_Sigma4("Tau_Sigma4", "@0 * @1",RooArgList(Tau_Sigma1,Tau_SigmaScale2));
	//RooRealVar    Tau_ResoFrac1("Tau_ResoFrac1","frac between double Gaussian resolution",0.8,0.78,0.82);//0.72
	RooRealVar    Tau_ResoFrac1("Tau_ResoFrac1","frac between double Gaussian resolution",0.8,0,1);//0.72
	//RooRealVar    Tau_ResoFrac2("Tau_ResoFrac2","frac between 3x Gaussian resolution",0.64,0,1);
	RooRealVar    Tau_ResoFrac2("Tau_ResoFrac2","frac between 3x Gaussian resolution",0.64,0,1);
	//RooRealVar    Tau_ResoFrac3("Tau_ResoFrac3","frac between 4x Gaussian resolution",0.5,0,1);
	RooRealVar    Tau_ResoFrac3("Tau_ResoFrac3","frac between 4x Gaussian resolution",1,0,1);
	// Lifetimes for b-decays and prompt (e.g. 0)
	RooRealVar Life_NP_1S("Life_NP_1S","",1.2,0.8,3.0);
	RooRealVar Life_NP_2S("Life_NP_2S","",1.2,0.8,7.0);
	RooRealVar Life_P("Life_P","",0.0);

	//Life_NP_1S.setConstant(kTRUE);

	//Life_NP_2S.setConstant(kTRUE);

	Tau_Sigma1.setConstant(kTRUE);
	//Tau_ResoFrac.setConstant(kTRUE);
	//Tau_ResoFrac1.setConstant(kTRUE);
	//Tau_ResoFrac2.setConstant(kTRUE);
	//Tau_ResoFrac3.setConstant(kTRUE);
	Tau_SigmaScale.setConstant(kTRUE);
	Tau_SigmaScale1.setConstant(kTRUE);
	//Tau_SigmaScal2.setConstant(kTRUE);
	Tau_Mean1.setConstant(kTRUE);
	Tau_Mean2.setConstant(kTRUE);
	Tau_Mean3.setConstant(kTRUE);

	if (m_fix_decay) {
		Tau_Sigma1.setVal(m_tau_sigma);
		Life_NP_1S.setVal(m_life_1s);
		Tau_Sigma1.setConstant(kTRUE);
		Life_NP_1S.setConstant(kTRUE);
	}

	RooRealVar Life_NP_1S_Short("Life_NP_1S_Short","",0.4,0.3,0.5);
	//RooRealVar Life_NP_1S_Medium("Life_NP_1S_Medium","",1.35,1.1,1.45);
	//RooRealVar Life_NP_1S_Long("Life_NP_1S_Long","",1.5,1.45,1.6);
	RooRealVar Life_NP_1S_Long("Life_NP_1S_Long","",1.3,1.0,2.0);
	//RooRealVar Life_NP_1S_LongScale("Life_NP_1S_LongScale","",2.0,1.0,3.0);
	Life_NP_1S_Short.setConstant(kTRUE);
	//Life_NP_1S_LongScale.setConstant(kTRUE);
	//RooFormulaVar Life_NP_1S_Long("Life_NP_1S_Long","Life_NP_1S_LongScale*Life_NP_1S_Short",RooArgSet(Life_NP_1S_LongScale,Life_NP_1S_Short));
	//RooRealVar f_Life_NP_1S_Short("f_Life_NP_1S_Short","",0.5,0.0,1.0);
	RooRealVar f_Life_NP_1S_Short("f_Life_NP_1S_Short","",0.5,-0.2,1.2);
	RooRealVar f_Life_NP_1S_Medium("f_Life_NP_1S_Medium","",0.5,0.0,1.0);

	RooGaussModel* Tau_Reso1 = new RooGaussModel("Tau_Reso1", "", *tau, Tau_Mean, Tau_Sigma1);
	//RooGaussModel* Tau_Reso2 = new RooGaussModel("Tau_Reso2", "", *tau, Tau_Mean, Tau_Sigma2);
	RooGaussModel* Tau_Reso2 = new RooGaussModel("Tau_Reso2", "", *tau, Tau_Mean, Tau_Sigma2);
	RooGaussModel* Tau_Reso3 = new RooGaussModel("Tau_Reso3", "", *tau, Tau_Mean, Tau_Sigma3);
	RooGaussModel* Tau_Reso4 = new RooGaussModel("Tau_Reso4", "", *tau, Tau_Mean, Tau_Sigma4);
	RooResolutionModel* Tau_Reso_2G = new RooAddModel("Tau_Reso_2G","double Gaussian Tau Reso",RooArgList(*Tau_Reso1,*Tau_Reso2),Tau_ResoFrac1);
	//mPDF_Tau_Reso = new RooAddModel("Tau_Reso_2G","double Gaussian Tau Reso",RooArgList(*Tau_Reso1,*Tau_Reso2),RooArgSet(Tau_ResoFrac1));
	//RooResolutionModel* Tau_Reso_3G = new RooAddModel("Tau_Reso_3G","triple Gaussian Tau Reso",RooArgList(*Tau_Reso_2G,*Tau_Reso3),RooArgSet(Tau_ResoFrac2));
	mPDF_Tau_Reso = new RooAddModel("mPDF_Tau_Reso","triple Gaussian Tau Reso",RooArgList(*Tau_Reso_2G,*Tau_Reso3),Tau_ResoFrac2);
	//mPDF_Tau_Reso = new RooAddModel("mPDF_Tau_Reso","4 Gaussian Tau Reso",RooArgList(*Tau_Reso_3G,*Tau_Reso4),RooArgSet(Tau_ResoFrac3));
	//RooRealVar    Tau_f1("Tau_f1","Tau_f1",0.4);
	//RooRealVar    Tau_f2("Tau_f2","Tau_f2",1.2);
	//RooRealVar    Tau_f3("Tau_f3","Tau_f3",1.2);
	//RooRealVar    Tau_f4("Tau_f4","Tau_f4",1.2);
	//mPDF_Tau_Reso = new RooAddModel("mPDF_Tau_Reso","triple Gaussian Tau Reso",RooArgList(*Tau_Reso1,*Tau_Reso2,*Tau_Reso3,*Tau_Reso4),RooArgSet(Tau_f1,Tau_f2,Tau_f3,Tau_f4));
	RooDecay mPDF_Tau_NP_1S_Short("mPDF_Tau_P_1S_Short","", *tau, Life_NP_1S_Short, *mPDF_Tau_Reso, RooDecay::SingleSided);
	//RooDecay mPDF_Tau_NP_1S_Medium("mPDF_Tau_P_1S_Medium","", *tau, Life_NP_1S_Medium, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooDecay mPDF_Tau_NP_1S_Long( "mPDF_Tau_P_1S_Long","",  *tau, Life_NP_1S_Long,  *mPDF_Tau_Reso, RooDecay::SingleSided);
	//f_Life_NP_1S_Short.setConstant(kTRUE);
	//RooAbsPdf* mPDF_Tau_NP_1S_doub = new RooAddPdf("mPDF_Tau_NP_1S_doub","",RooArgSet(mPDF_Tau_NP_1S_Short,mPDF_Tau_NP_1S_Medium),f_Life_NP_1S_Short); //Part of triple
	//mPDF_Tau_NP_1S = new RooAddPdf("mPDF_Tau_NP_1S","",RooArgSet(*mPDF_Tau_NP_1S_doub,mPDF_Tau_NP_1S_Long),f_Life_NP_1S_Medium); // Triple
	RooRealVar k_1S("k_1S","",0.4,0.05,1.0);
	RooRealVar k_f("k_f","",0.5,0.,4.0);
	k_1S.setConstant(kTRUE);
	//k_f.setConstant(kTRUE);
	//RooRealVar k_2S("k_2S","",0.5,0.1,0.8);
	RooFormulaVar k_2S("k_2S","k_f*k_1S",RooArgSet(k_f,k_1S));

	RooRealVar fk_1S("fk_1S","",0.5,0.0,1.0);
	RooRealVar fk_2S("fk_2S","",0.5,0.0,1.0);

	RooFormulaVar f1_1S("f1_1S","(1.0-@0)/3",RooArgSet(fk_1S));
	//RooRealVar f1_1S("f1_1S","",1./3);
	//RooRealVar f2_1S("f2_1S","",1./3);
	RooFormulaVar f2_1S("f2_1S","(1.0+@0)/3",RooArgSet(fk_1S));

	RooFormulaVar f1_2S("f1_2S","(1.0-@0)/3",RooArgSet(fk_2S));
	//RooRealVar f1_2S("f1_2S","",1./3);
	//RooRealVar f2_2S("f2_2S","",1./3);
	RooFormulaVar f2_2S("f2_2S","(1.0+@0)/3",RooArgSet(fk_2S));

	RooRealVar f3_1S("f3_1S","",1.0/3);
	//RooRealVar f3_1S("f3_1S","",0.5,0,1);
	RooRealVar f3_2S("f3_2S","",1.0/3);

	RooRealVar f_tau_1S("f_tau_1S","",0.5,0.0,1.0);
	RooRealVar f_tau_2S("f_tau_2S","",0.5,0.0,1.0);


	//f1_1S.setConstant(kTRUE);
	//f1_2S.setConstant(kTRUE);
	//f3_1S.setConstant(kTRUE);
	//f3_2S.setConstant(kTRUE);

	//RooFormulaVar Life_NP_1S_1("Life_NP_1S_1","@0*1.2",RooArgList(Life_NP_1S));
	//RooFormulaVar Life_NP_2S_1("Life_NP_2S_1","@0*1.2",RooArgList(Life_NP_2S));
	RooFormulaVar Life_NP_1S_1("Life_NP_1S_1","@0*(1-@1*0.707)",RooArgList(Life_NP_1S,k_1S));
	RooFormulaVar Life_NP_2S_1("Life_NP_2S_1","@0*(1-@1*0.707)",RooArgList(Life_NP_2S,k_2S));
	RooFormulaVar Life_NP_1S_2("Life_NP_1S_2","@0*(1+@1*0.707)",RooArgList(Life_NP_1S,k_1S));
	RooFormulaVar Life_NP_2S_2("Life_NP_2S_2","@0*(1+@1*0.707)",RooArgList(Life_NP_2S,k_2S));

	RooAbsPdf* mPDF_Tau_NP_1S_0 = new RooDecay("mPDF_Tau_NP_1S_0","", *tau, Life_NP_1S, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooAbsPdf* mPDF_Tau_NP_2S_0 = new RooDecay("mPDF_Tau_NP_2S_0","", *tau, Life_NP_2S, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooAbsPdf* mPDF_Tau_NP_1S_1 = new RooDecay("mPDF_Tau_NP_1S_1","", *tau, Life_NP_1S_1, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooAbsPdf* mPDF_Tau_NP_2S_1 = new RooDecay("mPDF_Tau_NP_2S_1","", *tau, Life_NP_2S_1, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooAbsPdf* mPDF_Tau_NP_1S_2 = new RooDecay("mPDF_Tau_NP_1S_2","", *tau, Life_NP_1S_2, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooAbsPdf* mPDF_Tau_NP_2S_2 = new RooDecay("mPDF_Tau_NP_2S_2","", *tau, Life_NP_2S_2, *mPDF_Tau_Reso, RooDecay::SingleSided);

	RooRealVar wt_1("wt_1","",0.3,0,1);
	RooRealVar wt_2("wt_2","",0.5,0,1);

	mPDF_Tau_NP_1S = new RooAddPdf("mPDF_Tau_NP_1S","",RooArgList(*mPDF_Tau_NP_1S_1,*mPDF_Tau_NP_1S_2,*mPDF_Tau_NP_1S_0),RooArgList(f1_1S,f2_1S,f3_1S));
	mPDF_Tau_NP_2S = new RooAddPdf("mPDF_Tau_NP_2S","",RooArgList(*mPDF_Tau_NP_2S_1,*mPDF_Tau_NP_2S_2,*mPDF_Tau_NP_2S_0),RooArgList(f1_2S,f2_2S,f3_2S));

	//RooAbsPdf* mPDF_Tau_NP_1S_tmp = new RooAddPdf("mPDF_Tau_NP_1S_tmp","",RooArgList(*mPDF_Tau_NP_1S_1,*mPDF_Tau_NP_1S_2),wt_1);
	//mPDF_Tau_NP_1S = new RooAddPdf("mPDF_Tau_NP_1S","",RooArgList(*mPDF_Tau_NP_1S_tmp,*mPDF_Tau_NP_1S_0),wt_2);

	//mPDF_Tau_NP_1S = mPDF_Tau_NP_1S_0;
	//mPDF_Tau_NP_2S = mPDF_Tau_NP_2S_0;
	mPDF_Tau_NP_1S = new RooDecay("mPDF_Tau_NP_1S","", *tau, Life_NP_1S, *mPDF_Tau_Reso, RooDecay::SingleSided);
	mPDF_Tau_NP_2S = new RooDecay("mPDF_Tau_NP_2S","", *tau, Life_NP_2S, *mPDF_Tau_Reso, RooDecay::SingleSided);
	if(sys == 99){
		mPDF_Tau_NP_1S = new RooAddPdf("mPDF_Tau_NP_1S","", RooArgList(*mPDF_Tau_NP_1S_0, *mPDF_Tau_NP_1S_1), f_tau_1S);
		mPDF_Tau_NP_2S = new RooAddPdf("mPDF_Tau_NP_2S","", RooArgList(*mPDF_Tau_NP_2S_0, *mPDF_Tau_NP_2S_1), f_tau_2S);
	}

	////////////////////////////////////////////////////////
	/// TIME BKG PDF
	////////////////////////////////////////////////////////
	RooRealVar Life_NP_B1   ("Life_NP_B1",    "", 1.2, 0.08, 3.0);
	RooRealVar Life_NP_B11   ("Life_NP_B11",    "", 1.2, 0.08, 6.0);
	RooRealVar Life_NP_MisID("Life_NP_MisID", "", 0.3, 0.1,  4.0);
	mPDF_Tau_NP_B1 = new RooDecay("t_bkg_decaySS", "", *tau, Life_NP_B1,    *mPDF_Tau_Reso, RooDecay::SingleSided);
	mPDF_Tau_NP_B11 = new RooDecay("t_bkg1_decaySS", "", *tau, Life_NP_B11,    *mPDF_Tau_Reso, RooDecay::SingleSided);
	mPDF_Tau_NP_B2 = new RooDecay("t_bkg_decayDS", "", *tau, Life_NP_MisID, *mPDF_Tau_Reso, RooDecay::DoubleSided);

	RooAbsPdf *mPDF_Tau_NP_B2_1, *mPDF_Tau_NP_B2_2;
	RooFormulaVar Life_NP_MisID_R("Life_NP_MisID_R","Life_NP_MisID/2",RooArgList(Life_NP_MisID));
	RooRealVar f_MisID_RL("f_MisID_RL", "", 0.5);
	if(sys==12){
		mPDF_Tau_NP_B2_1 = new RooDecay("t_bkg_decayDS_1", "", *tau, Life_NP_MisID, *mPDF_Tau_Reso, RooDecay::Flipped);
		mPDF_Tau_NP_B2_2 = new RooDecay("t_bkg_decayDS_2", "", *tau, Life_NP_MisID_R, *mPDF_Tau_Reso, RooDecay::SingleSided);
		mPDF_Tau_NP_B2 = new RooAddPdf("t_bkg_decayDS","",RooArgList(*mPDF_Tau_NP_B2_1,*mPDF_Tau_NP_B2_2),f_MisID_RL);
	}

	//RooGenericPdf mPDF_Tau_Double_Exp("mPDF_Tau_Double_Exp","(@0<0) ? exp(-abs(@0/Life_NP_MisID)) : 0",RooArgSet(*tau,Life_NP_MisID));
	//mPDF_Tau_NP_B2 = new RooFFTConvPdf("t_bkg_decayDS","",*tau,*mPDF_Tau_Reso,mPDF_Tau_Double_Exp);

	//mPDF_Tau_NP_B2 = new RooDecay("t_bkg_decayDS", "", *tau, Life_NP_MisID, *mPDF_Tau_Reso, RooDecay::DoubleSided);

	////////////////////////////////////////////////////////
	/// Invariance Mass Signal for Jpsi and Psi(2S)
	////////////////////////////////////////////////////////
	RooRealVar Mass_Mean_1S("Mass_Mean_1S","",3.096,3.07,3.12);
	RooRealVar Mass_SigmaScal_2S("Mass_SigmaScal_2S","",3.686/3.097,1.07,2.26);
	Mass_SigmaScal_2S.setConstant(kTRUE);
	RooFormulaVar Mass_Mean_2S("Mass_Mean_2S","@0 * @1",RooArgList(Mass_Mean_1S,Mass_SigmaScal_2S));

	//RooRealVar corr("corr","",1,-10,10);
	//RooFormulaVar Mass_Sigma_1S_G("Mass_Sigma_1S_G","corr*Tau_Sigma1",RooArgSet(corr,Tau_Sigma1));
	RooRealVar Mass_Sigma_1S_G("Mass_Sigma_1S_G","",0.035,0.02,0.25);

	if(iy==0)
		Mass_Sigma_1S_G.setVal(25*1e-3+pt*0.27*1e-3); //6*1e-3+0.08*1e-3
	if(iy==1)
		Mass_Sigma_1S_G.setVal(25*1e-3+pt*0.46*1e-3);
	if(iy==2)
		Mass_Sigma_1S_G.setVal(25*1e-3+pt*0.70*1e-3);

	Mass_Sigma_1S_G.setRange(0.4*Mass_Sigma_1S_G.getValV(), 1.4*Mass_Sigma_1S_G.getValV());

	//Mass_Sigma_1S_G.setConstant(kTRUE);

	//RooRealVar Mass_CB_SigmaScale("Mass_CB_SigmaScale","",1.70,1.0,2.5);
	RooRealVar Mass_CB_SigmaScale("Mass_CB_SigmaScale","",1.70,1.0,4.5);
	RooFormulaVar Mass_Sigma_1S_CB("Mass_Sigma_1S_CB","Mass_CB_SigmaScale*Mass_Sigma_1S_G",RooArgSet(Mass_CB_SigmaScale,Mass_Sigma_1S_G));
	RooRealVar Mass_CBn("Mass_CBn","",2.0,0.2,3.0);
	Mass_CBn.setConstant(kTRUE);
	RooRealVar Mass_CBa("Mass_CBa","",1.5,1.0,3.0);
	Mass_CBa.setConstant(kTRUE);
	Mass_CB_SigmaScale.setConstant(kTRUE);

	RooFormulaVar Mass_Sigma_2S_G("Mass_Sigma_2S_G","@0 * @1",RooArgList(Mass_Sigma_1S_G,Mass_SigmaScal_2S));
	RooFormulaVar Mass_Sigma_2S_CB("Mass_Sigma_2S_CB","Mass_CB_SigmaScale*Mass_Sigma_2S_G",RooArgSet(Mass_CB_SigmaScale,Mass_Sigma_2S_G));
	RooRealVar Mass_GaussFrac("Mass_GaussFrac","",0.75,0.5,1.0);
	Mass_GaussFrac.setConstant(kTRUE);

	RooCBShape PDF_Mass_Signal_1S_CB("PDF_Mass_Signal_1S_CB","1S Signal CB", *m, Mass_Mean_1S, Mass_Sigma_1S_CB, Mass_CBa, Mass_CBn);
	RooCBShape PDF_Mass_Signal_2S_CB("PDF_Mass_Signal_2S_CB","2S Signal CB", *m, Mass_Mean_2S, Mass_Sigma_2S_CB, Mass_CBa, Mass_CBn);

	RooGaussian PDF_Mass_Signal_1S_G("PDF_Mass_Signal_1S_G","1S Signal G", *m, Mass_Mean_1S, Mass_Sigma_1S_G);
	RooGaussian PDF_Mass_Signal_2S_G("PDF_Mass_Signal_2S_G","2S Signal G", *m, Mass_Mean_2S, Mass_Sigma_2S_G);

	mPDF_Mass_Signal_1S = new RooAddPdf("PDF_Mass_Signal_1S","",RooArgList(PDF_Mass_Signal_1S_G,PDF_Mass_Signal_1S_CB),Mass_GaussFrac);
	mPDF_Mass_Signal_2S = new RooAddPdf("PDF_Mass_Signal_2S","",RooArgList(PDF_Mass_Signal_2S_G,PDF_Mass_Signal_2S_CB),Mass_GaussFrac);
	////////////////////////////////////////////////////////
	/// Mass Bkgd
	////////////////////////////////////////////////////////
	RooRealVar Bkgd_Exp_Prompt("Bkgd_Exp_Prompt","",-1,-12.0,0.);
	RooRealVar Bkgd_Exp_MisID( "Bkgd_Exp_MisID", "",-1,-8.0,0.);
	RooRealVar Bkgd_Exp_MisID1( "Bkgd_Exp_MisID1", "",0,-8.,8.);
	//RooRealVar Bkgd_Exp_NonPrompt("Bkgd_Exp_NonPrompt","",-0.5,-10,1);
	RooRealVar Bkgd_Mean( "Bkgd_Mean","",2.6*0.95,1.8,2.6);
	RooRealVar Bkgd_Sigma("Bkgd_Sigma","",0.7,0.5,2.0);
	Bkgd_Sigma.setConstant(kTRUE);
	Bkgd_Mean.setConstant(kTRUE);

	RooRealVar Bkgd_Pow1("Bkgd_Pow1","",3.5,1,11);
	//Bkgd_Pow1.setConstant(kTRUE);
	RooRealVar Bkgd_Const("Bkgd_Const","",0,0,300);
	//Bkgd_Const.setConstant(kTRUE);
	RooRealVar Bkgd_Par("Bkgd_Par","",5,3.51,7);
	//Bkgd_Par.setConstant(kTRUE);
	RooRealVar Bkgd_Exp_NonPrompt("Bkgd_Exp_NonPrompt","",-2.,-3.,-1.);
	//Bkgd_Exp_NonPrompt.setConstant(kTRUE);
	RooRealVar Bkgd_Pow2("Bkgd_Pow2","",5,1,14);
	//Bkgd_Pow2.setConstant(kTRUE);
	RooRealVar NP_Vato_Frac("NP_Vato_Frac","frac between Vato and Exp",1,0,1);
	//NP_Vato_Frac.setConstant(kTRUE);

	RooFormulaVar mShift("mShift","m-2.6",RooArgList(*m));

	mPDF_Mass_Bkgd_Prompt = new RooExponential("PDF_Mass_Bkgd_Prompt","Bkgd.", *m, Bkgd_Exp_Prompt);

	//mPDF_Mass_Bkgd_Prompt = new RooExponential("PDF_Mass_Bkgd_Prompt","Bkgd.", mShift, Bkgd_Exp_Prompt);
	//RooRealVar mean_bkgd_prompt("Bkgd_Exp_Prompt_Mean", "", 2, 0, 10);
	//RooRealVar sigma_bkgd_prompt("Bkgd_Exp_Prompt_Sigma", "", 5, 1, 10);
	//mPDF_Mass_Bkgd_Prompt = new RooGaussian("mPDF_Mass_Bkgd_Prompt", "Bkgd.", *m, mean_bkgd_prompt, sigma_bkgd_prompt);

	RooRealVar Bkgd_P_p1("Bkgd_P_p1","",0.1,0,1);
	RooRealVar Bkgd_P_p2("Bkgd_P_p2","",0.9,0,1);
	RooRealVar Bkgd_P_p3("Bkgd_P_p3","",0.2,0,1);
	RooRealVar Bkgd_P_p4("Bkgd_P_p4","",0.8,0,1);
	RooRealVar Bkgd_P_p5("Bkgd_P_p5","",0.5,0,1);
	//mPDF_Mass_Bkgd_Prompt = new RooPolynomial("PDF_Mass_Bkgd_Prompt","Bkgd.", mShift);
	//mPDF_Mass_Bkgd_Prompt = new RooPolynomial("PDF_Mass_Bkgd_Prompt","Bkgd.", mShift, RooArgList(Bkgd_P_p1));
	if(sys!=10)
		mPDF_Mass_Bkgd_Prompt = new RooBernstein("PDF_Mass_Bkgd_Prompt","Bkgd.", *m, RooArgList(Bkgd_P_p1,Bkgd_P_p2,Bkgd_P_p3));
	mPDF_Mass_Bkgd_MisID  = new RooExponential("PDF_Mass_Bkgd_MisID", "Bkgd.", mShift, Bkgd_Exp_MisID);
	mPDF_Mass_Bkgd_MisID1  = new RooExponential("PDF_Mass_Bkgd_MisID1", "Bkgd.", mShift, Bkgd_Exp_MisID1);
	//mPDF_Mass_Bkgd_NonPrompt = new RooGaussian("PDF_Mass_Bkgd_NonPrompt","Bkgd.",*m,Bkgd_Mean,Bkgd_Sigma);
	//mPDF_Mass_Bkgd_NonPrompt  = new RooExponential("PDF_Mass_Bkgd_NonPrompt", "Bkgd.", *m, Bkgd_Exp_NonPrompt);
	//mPDF_Mass_Bkgd_NonPrompt = new RooGenericPdf("PDF_Mass_Bkgd_NonPrompt","","Bkgd_Const*exp(-Bkgd_Exp_NonPrompt*(m-4.2))+(0.02*pow(m,Bkgd_Pow1)*pow((Bkgd_Par-m),Bkgd_Pow2))",RooArgSet(*m,Bkgd_Pow1,Bkgd_Const,Bkgd_Par,Bkgd_Exp_NonPrompt,Bkgd_Pow2));
	//RooAbsPdf* mPDF_Mass_Bkgd_NonPrompt_Vato = new RooGenericPdf("PDF_Mass_Bkgd_NonPrompt_Vato","","(pow(m,Bkgd_Pow1)*pow((Bkgd_Par-m),Bkgd_Pow2))",RooArgSet(*m,Bkgd_Pow1,Bkgd_Par,Bkgd_Pow2));
	///*
	RooRealVar Bkgd_m_exp_c("Bkgd_m_exp_c","",0.1,-1,1);
	RooRealVar Bkgd_m_cosh_c("Bkgd_m_cosh_c","",0,0,1);
	RooRealVar Bkgd_m_cosh_c1("Bkgd_m_cosh_c1","",1,0,3);
	RooRealVar Bkgd_m_exp1_c("Bkgd_m_exp1_c","",-0.09,-0.2,1);
	RooRealVar Bkgd_m_exp1_c1("Bkgd_m_exp1_c1","",3.1,3.096,3.2);
	RooAbsPdf* mPDF_Mass_Bkgd_NonPrompt_Vato = new RooGenericPdf("PDF_Mass_Bkgd_NonPrompt_Vato","","Bkgd_m_exp_c*exp(-m)+Bkgd_m_cosh_c/cosh(Bkgd_m_cosh_c1*(m-3.096))+exp(Bkgd_m_exp1_c*(m-Bkgd_m_exp1_c1))",RooArgSet(*m,Bkgd_m_exp_c,Bkgd_m_cosh_c,Bkgd_m_cosh_c1,Bkgd_m_exp1_c,Bkgd_m_exp1_c1));
	//*/
	//RooFormulaVar A("Shift_NP_M_Bkg","m-3.5",RooArgSet(*m));
	RooAbsPdf* mPDF_Mass_Bkgd_NonPrompt_Exp = new RooExponential("PDF_Mass_Bkgd_NonPrompt_Exp", "Bkgd. ", mShift, Bkgd_Exp_NonPrompt);
	//mPDF_Mass_Bkgd_NonPrompt = new RooAddPdf("mPDF_Mass_Bkgd_NonPrompt","",RooArgSet(*mPDF_Mass_Bkgd_NonPrompt_Vato,*mPDF_Mass_Bkgd_NonPrompt_Exp), RooArgSet(NP_Vato_Frac));
	//mPDF_Mass_Bkgd_NonPrompt = mPDF_Mass_Bkgd_NonPrompt_Vato; //Only Vato

	RooRealVar Bkgd_NP_p1("Bkgd_NP_p1","",0.1,0,1);
	RooRealVar Bkgd_NP_p2("Bkgd_NP_p2","",0.9,0,1);
	RooRealVar Bkgd_NP_p3("Bkgd_NP_p3","",0.2,0,1);
	RooRealVar Bkgd_NP_p4("Bkgd_NP_p4","",0.8,0,1);
	RooRealVar Bkgd_NP_p5("Bkgd_NP_p5","",0.5,0,1);
	//mPDF_Mass_Bkgd_Prompt = new RooPolynomial("PDF_Mass_Bkgd_Prompt","Bkgd.", mShift, RooArgList(Bkgd_P_p1,Bkgd_P_p2));

	mPDF_Mass_Bkgd_NonPrompt = mPDF_Mass_Bkgd_NonPrompt_Exp;
	if(sys==11)
		mPDF_Mass_Bkgd_NonPrompt = new RooBernstein("PDF_Mass_Bkgd_NonPrompt","Bkgd.", *m, RooArgList(Bkgd_NP_p1,Bkgd_NP_p2,Bkgd_NP_p3));

	RooRealVar mean_bkgd_nonprompt("Bkgd_Exp_NPrompt_Mean", "", 2, 0, 3.0);
	RooRealVar sigma_bkgd_nonprompt("Bkgd_Exp_NPrompt_Sigma", "", 5, 0, 10);
	if(sys==99){
		mPDF_Mass_Bkgd_NonPrompt = new RooGaussian("mPDF_Mass_Bkgd_NonPrompt", "Bkgd.", *m, mean_bkgd_nonprompt, sigma_bkgd_nonprompt);
	}

	//*************TEST******************//
	RooRealVar Bkgd_m_argpar("Bkgd_m_argpar","argus shape parameter",-20.0,-100.,-1.);
	RooRealVar Bkgd_m_m0("Bkgd_m_m0","argus m0",3.0,2.7,3.5);
	RooArgusBG argus("argus","Argus PDF",*m,Bkgd_m_m0,Bkgd_m_argpar);

	RooRealVar a0("a0","a0",-0.1,-1,1) ;
	RooRealVar a1("a1","a1",0.0,-1,1) ;
	RooRealVar a2("a2","a2",0.0,-1,1) ;
	RooChebychev* chebyshev = new RooChebychev("chebyshev","chebyshev PDF",*m,RooArgSet(a0,a1,a2)) ;
	//mPDF_Mass_Bkgd_NonPrompt = new RooAddPdf("mPDF_Mass_Bkgd_NonPrompt","",RooArgSet(argus,chebyshev), RooArgSet(NP_Vato_Frac));
	//mPDF_Mass_Bkgd_NonPrompt = (RooAbsPdf*)chebyshev;
	//**********************************//

	////////////////////////////////////////////////////////
	/// Build Complete Model
	////////////////////////////////////////////////////////
	RooRealVar f_NP_1S("f_NP_1S","1S non-prompt fraction", 0.7, 0.0, 1.0);
	RooRealVar f_NP_2S("f_NP_2S","2S non-prompt fraction", 0.7, 0.0, 1.0);
	RooRealVar f_NP_B_MisID("f_NP_B_MisID","Bkgd. NP Fraction",0.2,0.0,1.0); //0.2
	RooRealVar f_NP_B_MisID1("f_NP_B_MisID1","Bkgd. NP Fraction 1",0.04,0.0,1.0); //0.2
	RooRealVar f_NP_B("f_NP_B","Bkgd. NP Fraction",0.9,0.0,1.0); //0.2


	//RooRealVar corr("corr","",0,-1,1);
	//RooAbsPdf* PDF_P_1S_core = new RooProdPdf("PDF_P_1S_core", "", RooArgSet(*mPDF_Mass_Signal_1S,*mPDF_Tau_Reso) );
	//RooAbsPdf* PDF_P_1S_corr = new RooGenericPdf("PDF_P_1S_corr", "exp(-corr*(m-Mass_Mean_1S)*(tau-Tau_Mean)/(Mass_Sigma_1S_G*sqrt((Tau_ResoFrac1*Tau_Sigma1*Tau_Sigma1)+(1-Tau_ResoFrac1)*Tau_Sigma2*Tau_Sigma2)))", RooArgSet(*m,*tau,Mass_Mean_1S,Mass_Sigma_1S_G,Tau_Mean,Tau_Sigma1,corr,Tau_Sigma2,Tau_ResoFrac1));
	//RooAbsPdf* PDF_P_1S  = new RooProdPdf("PDF_P_1S", "", RooArgSet(*PDF_P_1S_core,*PDF_P_1S_corr) );

	//RooRealVar* angle = new RooRealVar("angle","",0,0,2*TMath::Pi());
	//RooRealVar* angle1 = new RooRealVar("angle1","",0,0,2*TMath::Pi());
	//angle->setConstant(kTRUE);
	//angle1->setConstant(kTRUE);
	RooRealVar* rho_1S = new RooRealVar("rho_1S","",0.33,-1.,1.);
	RooRealVar* rho_1S_1 = new RooRealVar("rho_1S_1","",0,-0.5,0.5);
	rho_1S->setConstant(kTRUE);
	rho_1S_1->setConstant(kTRUE);

	//RooFormulaVar a("a","cos(angle)*cos(angle)/(2*Mass_Sigma_1S_G*Mass_Sigma_1S_G)+sin(angle)*sin(angle)/(2*Tau_Sigma1*Tau_Sigma1)",RooArgSet(*angle,Mass_Sigma_1S_G,Tau_Sigma1));
	//RooFormulaVar b("b","-sin(2*angle)/(4*Mass_Sigma_1S_G*Mass_Sigma_1S_G)+sin(2*angle)/(4*Tau_Sigma1*Tau_Sigma1)",RooArgSet(*angle,Mass_Sigma_1S_G,Tau_Sigma1));
	//RooFormulaVar c("c","sin(angle)*sin(angle)/(2*Mass_Sigma_1S_G*Mass_Sigma_1S_G)+cos(angle)*cos(angle)/(2*Tau_Sigma1*Tau_Sigma1)",RooArgSet(*angle,Mass_Sigma_1S_G,Tau_Sigma1));

	////RooAbsPdf* PDF_P_1S = new RooEllipticGaussianPdf("PDF_P_1S","",*m,Mass_Mean_1S,*tau,Tau_Mean,a,b,c);

	////RooAbsPdf* PDF_P_1S  = new RooGenericPdf("PDF_P_1S", "", "exp( -( a*(m-Mass_Mean_1S)*(m-Mass_Mean_1S)+2*b*(m-Mass_Mean_1S)*(tau-Tau_Mean)+c*(tau-Tau_Mean)*(tau-Tau_Mean) ) )", RooArgSet(*tau,*m,Mass_Mean_1S,Tau_Mean,a,b,c) );
	//RooAbsPdf* PDF_P_1S_1  = new RooGenericPdf("PDF_P_1S_1", "", "exp( -( a*(m-Mass_Mean_1S)*(m-Mass_Mean_1S)+2*b*(m-Mass_Mean_1S)*(tau-Tau_Mean) ) )", RooArgSet(*tau,*m,Mass_Mean_1S,Tau_Mean,a,b) );
	//RooAbsPdf* PDF_P_1S_1  = new RooGenericPdf("PDF_P_1S_1", "", "exp( -( (m-Mass_Mean_1S)*(m-Mass_Mean_1S)/(2*Mass_Sigma_1S_G*Mass_Sigma_1S_G) ) )", RooArgSet(*m,Mass_Mean_1S,Mass_Sigma_1S_G) );
	//RooAbsPdf* PDF_P_1S_2  = new RooGenericPdf("PDF_P_1S_2", "", "exp( -( c*(tau-Tau_Mean)*(tau-Tau_Mean) ) )", RooArgSet(*tau,Tau_Mean,c) );
	//RooAbsPdf* PDF_P_1S_2  = new RooGenericPdf("PDF_P_1S_2", "", "exp( -( (tau-Tau_Mean)*(tau-Tau_Mean)/(2*Tau_Sigma1*Tau_Sigma1) ) )", RooArgSet(*tau,Tau_Mean,Tau_Sigma1) );
	//RooAbsPdf* PDF_P_1S  = new RooProdPdf("PDF_P_1S", "", RooArgSet(*PDF_P_1S_1,*PDF_P_1S_2) );
	//RooAbsPdf* PDF_P_1S  = new RooProdPdf("PDF_P_1S", "", RooArgSet(*mPDF_Mass_Signal_1S,*PDF_P_1S_2) );

	//RooAbsPdf* PDF_P_1S_1 = new RooBVNPdf("PDF_P_1S_1","",*m,Mass_Mean_1S,Mass_Sigma_1S_G,*tau,Tau_Mean,Tau_Sigma1,*angle);
	//RooAbsPdf* PDF_P_1S_2 = new RooBVNPdf("PDF_P_1S_2","",*m,Mass_Mean_1S,Mass_Sigma_1S_G,*tau,Tau_Mean,Tau_Sigma2,*angle1);
	RooAbsPdf* PDF_P_1S_1 = new RooBVNPdf("PDF_P_1S_1","",*m,Mass_Mean_1S,Mass_Sigma_1S_G,*tau,Tau_Mean,Tau_Sigma1,*rho_1S);
	//RooAbsPdf* PDF_P_1S_1 = new RooProdPdf("PDF_P_1S_1","",RooArgSet(PDF_Mass_Signal_1S_G,*Tau_Reso1));
	//RooAbsPdf* PDF_P_1S_2 = new RooBVNPdf("PDF_P_1S_2","",*m,Mass_Mean_1S,Mass_Sigma_1S_G,*tau,Tau_Mean,Tau_Sigma2,*rho_1S_1);
	RooAbsPdf* PDF_P_1S_2 = new RooProdPdf("PDF_P_1S_2","",RooArgSet(PDF_Mass_Signal_1S_G,*Tau_Reso2));
	RooAbsPdf* PDF_P_1S_3 = new RooProdPdf("PDF_P_1S_3","",RooArgSet(PDF_Mass_Signal_1S_G,*Tau_Reso3));
	RooAbsPdf* PDF_P_1S_CB = new RooProdPdf("PDF_P_1S_CB","", RooArgSet(PDF_Mass_Signal_1S_CB,*mPDF_Tau_Reso) );
	//RooAbsPdf* PDF_P_1S_4 = new RooBVNPdf("PDF_P_1S_4","",*m,Mass_Mean_1S,Mass_Sigma_1S_G,*tau,Tau_Mean,Tau_Sigma3,*rho_1S_1);
	RooAbsReal* w1 = new RooFormulaVar("w1","Mass_GaussFrac*Tau_ResoFrac1",RooArgSet(Mass_GaussFrac,Tau_ResoFrac1));
	RooAbsReal* w2 = new RooFormulaVar("w2","Mass_GaussFrac*Tau_ResoFrac2",RooArgSet(Mass_GaussFrac,Tau_ResoFrac2));
	//RooAbsReal* w2 = new RooFormulaVar("w2","Mass_GaussFrac*(1-Tau_ResoFrac1)",RooArgSet(Mass_GaussFrac,Tau_ResoFrac1));
	//RooAbsReal* w3 = new RooFormulaVar("w3","1-Mass_GaussFrac",RooArgSet(Mass_GaussFrac));
	//RooRealVar* w1 = new RooRealVar("w1","",0.5,0,1);
	RooAbsPdf* PDF_P_1S = NULL;

	if(angleOn && sys != 99){
		RooAbsPdf* PDF_P_1S_tmp1 = new RooAddPdf("P_1S_tmp1", "", RooArgList(*PDF_P_1S_1,*PDF_P_1S_2),Tau_ResoFrac1);
		RooAbsPdf* PDF_P_1S_tmp2 = new RooAddPdf("P_1S_tmp2", "", RooArgList(*PDF_P_1S_tmp1,*PDF_P_1S_3),Tau_ResoFrac2);
		PDF_P_1S = new RooAddPdf("PDF_P_1S", "", RooArgList(*PDF_P_1S_tmp2,*PDF_P_1S_CB),Mass_GaussFrac);
		//PDF_P_1S = new RooAddPdf("PDF_P_1S", "", RooArgSet(*PDF_P_1S_1,*PDF_P_1S_2,*PDF_P_1S_3),RooArgSet(*w1,*w2,*w3));
	} else{
		PDF_P_1S  = new RooProdPdf("PDF_P_1S", "", RooArgSet(*mPDF_Mass_Signal_1S,*mPDF_Tau_Reso) );
	}

	RooAbsPdf* PDF_P_2S  = new RooProdPdf("PDF_P_2S", "", RooArgSet(*mPDF_Mass_Signal_2S,*mPDF_Tau_Reso) );
	RooAbsPdf* PDF_NP_1S = new RooProdPdf("PDF_NP_1S","", RooArgSet(*mPDF_Mass_Signal_1S,*mPDF_Tau_NP_1S));
	RooAbsPdf* PDF_NP_2S = new RooProdPdf("PDF_NP_2S","", RooArgSet(*mPDF_Mass_Signal_2S,*mPDF_Tau_NP_2S));
	RooAbsPdf* PDF_1S = new RooAddPdf("PDF_1S", "",RooArgList(*PDF_NP_1S,*PDF_P_1S),f_NP_1S);
	RooAbsPdf* PDF_2S = new RooAddPdf("PDF_2S", "",RooArgList(*PDF_NP_2S,*PDF_P_2S),f_NP_2S);

	RooAbsPdf* PDF_P_B1  = new RooProdPdf("PDF_P_B1", "", RooArgSet(*mPDF_Mass_Bkgd_Prompt,   *mPDF_Tau_Reso) );      //bkgd1
	RooAbsPdf* PDF_NP_B1 = new RooProdPdf("PDF_NP_B1","", RooArgSet(*mPDF_Mass_Bkgd_NonPrompt,*mPDF_Tau_NP_B1));      //bkgd4
	RooAbsPdf* PDF_NP_B2 = new RooProdPdf("PDF_NP_B2","", RooArgSet(*mPDF_Mass_Bkgd_MisID,    *mPDF_Tau_NP_B2));      //bkgd3
	RooAbsPdf* PDF_NP_B21 = new RooProdPdf("PDF_NP_B21","", RooArgSet(*mPDF_Mass_Bkgd_MisID1,    *mPDF_Tau_NP_B11));  //bkgd2

	//RooAbsPdf* PDF_NP_Bs  = new RooAddPdf("PDF_NP_Bs", "",RooArgList(*PDF_NP_B2,*PDF_NP_B1),RooArgList(f_NP_B_MisID));
	//RooAbsPdf* PDF_NP_B_MisID  = new RooAddPdf("PDF_NP_B_MisID", "",RooArgList(*PDF_NP_B21,*PDF_NP_B2),RooArgList(f_NP_B_MisID1));
	//RooAbsPdf* PDF_NP_B  = new RooAddPdf("PDF_NP_B", "",RooArgList(*PDF_NP_B_MisID,*PDF_NP_B1),RooArgList(f_NP_B_MisID));

	RooAbsPdf* PDF_NP_B  = new RooAddPdf("PDF_NP_B", "",RooArgList(*PDF_NP_B2,*PDF_NP_B1),RooArgList(f_NP_B_MisID));
	//RooAbsPdf* PDF_NP_B  = (RooAbsPdf*)PDF_NP_B2->Clone();
	RooAbsPdf* PDF_B     = new RooAddPdf("PDF_B", "", RooArgSet(*PDF_NP_B,*PDF_P_B1),RooArgSet(f_NP_B));

	//RooRealVar N_1S("N_1S","",0.30*data->sumEntries(), 0.10*data->sumEntries(), 0.80*data->sumEntries());
	RooRealVar N_1S("N_1S","",0.30*data->sumEntries(), 0.10*data->sumEntries(), 1*data->sumEntries());
	RooRealVar N_2S("N_2S","",0.05*data->sumEntries(), 0.005*data->sumEntries(), 0.20*data->sumEntries());
	//RooRealVar N_B ("N_B", "",0.60*data->sumEntries(), 0.05*data->sumEntries(), 0.90*data->sumEntries());
	RooRealVar N_B ("N_B", "",0.60*data->sumEntries(), 0*data->sumEntries(), 0.90*data->sumEntries());

	//Fix psi2S for high pT
	RooFormulaVar *N_2S_fixed;
	RooRealVar N_2S_SF("N_2S_SF","",0.06,0.01,0.09);
	N_2S_SF.setConstant(kTRUE);
	if(pt>140){
		N_2S_fixed = new RooFormulaVar("N_2S","N_1S*N_2S_SF",RooArgSet(N_1S,N_2S_SF));
		f_NP_2S.setVal(0.7);
		//f_NP_2S.setRange(0.6,0.8);
		f_NP_2S.setConstant(kTRUE);
	}


	// Extended PDF
	RooAbsPdf* ePDF_1S = new RooExtendPdf("ePDF_1S","",*PDF_1S,N_1S);
	RooAbsPdf* ePDF_2S;
	if(pt>140)
		ePDF_2S = new RooExtendPdf("ePDF_2S","",*PDF_2S,*N_2S_fixed);
	else
		ePDF_2S = new RooExtendPdf("ePDF_2S","",*PDF_2S,N_2S);
	RooAbsPdf* ePDF_B  = new RooExtendPdf("ePDF_B", "",*PDF_B, N_B );

	// Derived Yields
	RooFormulaVar N_1S_P ("N_1S_P", "N_1S*(1.0-f_NP_1S)",RooArgSet(N_1S,f_NP_1S));
	RooFormulaVar N_1S_NP("N_1S_NP","N_1S*f_NP_1S",      RooArgSet(N_1S,f_NP_1S));
	RooFormulaVar N_2S_P ("N_2S_P", "N_2S*(1.0-f_NP_2S)",RooArgSet(N_2S,f_NP_2S));
	RooFormulaVar N_2S_NP("N_2S_NP","N_2S*f_NP_2S",      RooArgSet(N_2S,f_NP_2S));

	// Total Extended PDF
	RooAbsPdf* model = new RooAddPdf("ePDF_Total","",RooArgSet(*ePDF_1S,*ePDF_2S,*ePDF_B));
	/*
	   RooBinning* MassBinning = new RooBinning(2.6, 4.2,"MassBinning");
	   MassBinning->addUniform(80, 2.6, 4.2);
	   m->setBinning(*MassBinning);

	   RooBinning* TauBinning = new RooBinning(-1.5, 15,"TauBinning");
	   TauBinning->addUniform(400, -1.5, 15);
	   tau->setBinning(*TauBinning);
	   */
	//tau->setBins(40);
	RooDataHist* BinnedDataSample = data->binnedClone("BinnedDataSample","");
	//RooDataSet* BinnedDataSample = data;
	data->Print();
	BinnedDataSample->Print();

	std::cout << "(Post Cut) DataSample numEntries = " << data->numEntries() << " " << data->sumEntries() << std::endl;
	std::cout << "(Post Cut) BinnedDataSample numEntries = " << BinnedDataSample->numEntries() << " " << BinnedDataSample->sumEntries() << std::endl;

	/*TIterator* iter = params->createIterator() ;
	  while((parg=(RooRealVar*)iter->Next())) {
	  cout<<parg->getValV()<<endl;
	  }*/

	//RooDataSet* BinnedDataSample = (RooDataSet*)data->Clone();

	//////////////////////////////////////////
	/// Mass 1D
	//////////////////////////////////////////
	RooAbsPdf* M1D_PDF_1S = mPDF_Mass_Signal_1S;
	RooAbsPdf* M1D_PDF_2S = mPDF_Mass_Signal_2S;
	RooAbsPdf* M1D_PDF_B  = mPDF_Mass_Bkgd_Prompt;

	// Extended PDF
	RooAbsPdf* M1D_ePDF_1S = new RooExtendPdf("M1D_ePDF_1S","",*M1D_PDF_1S,N_1S);
	RooAbsPdf* M1D_ePDF_2S = new RooExtendPdf("M1D_ePDF_2S","",*M1D_PDF_2S,N_2S);
	RooAbsPdf* M1D_ePDF_B  = new RooExtendPdf("M1D_ePDF_B", "",*M1D_PDF_B, N_B );

	// Total Extended PDF
	RooAbsPdf* M1D_model = new RooAddPdf("M1D_ePDF_Total","",RooArgSet(*M1D_ePDF_1S,*M1D_ePDF_2S,*M1D_ePDF_B));

	//-----------------------------------------

	//Extract params
	RooArgSet* params = model->getParameters(*data);
	TIterator *iterat= savedParams->createIterator();
	RooAbsArg *next = 0;
	//if( 0 != (next= (RooAbsArg*)iterat->Next()) && sys<0){//Disabled for now
	if( 0 != (next= (RooAbsArg*)iterat->Next()) ){
		*params = *savedParams;
	}
	params->Print("v");

	////////////////////////////////////////////////////////
	/// Fit data
	////////////////////////////////////////////////////////
	int cores = atoi(gSystem->GetFromPipe("nproc"));

	if(useInput || drawOnly){
		params->readFromFile(inputF->Data());
		//tmp
		/*
		   int cnt = 0;
		   TIterator* iter = params->createIterator() ;
		   RooRealVar *parg = 0;
		   while((parg=(RooRealVar*)iter->Next())) {
		   if(!(cnt==6 || cnt==30))
		   parg->setConstant(kTRUE);
		   cnt++;
		   }
		   */
		if(drawOnly){
			TIterator* iter = params->createIterator() ;
			RooRealVar *parg = 0;
			while((parg=(RooRealVar*)iter->Next())) {
				parg->setConstant(kTRUE);
			}
		}

		if(pt>140){
			f_NP_2S.setVal(0.7);
			//f_NP_2S.setRange(0.6,0.8);
			f_NP_2S.setConstant(kTRUE);
		}
		N_1S.setVal(0.30*data->sumEntries());
		N_1S.setRange(0.10*data->sumEntries(),1*data->sumEntries());

		params->Print("v");
		//cin.get();
	}else{
		//RooDataSet* mass1D = (RooDataSet*)BinnedDataSample->reduce(SelectVars(RooArgSet(*m)));
		RooDataSet* mass1D = (RooDataSet*)data->reduce(SelectVars(RooArgSet(*m)));
		{
			//m_result = M1D_model->fitTo(*mass1D,NumCPU(cores),Strategy(2),Save(kTRUE),SumW2Error(kTRUE),Timer(kTRUE));
			//RooAbsReal* nll = M1D_model->createNLL(*mass1D,NumCPU(cores),Offset(1));
			RooAbsReal* nll = M1D_model->createNLL(*mass1D,Offset(1));

			RooMinimizer min(*nll);
			min.setStrategy(2);

			min.optimizeConst(kTRUE);
			min.minimize("Minuit2","Migrad");
			min.hesse();
			min.save()->Print("v");
		}
		double sf = 6.5;
		if(badFit)
			sf = 7;

		double lower_1S = Mass_Mean_1S.getValV()-sf*Mass_Sigma_1S_G.getValV();
		double upper_1S = Mass_Mean_1S.getValV()+sf*Mass_Sigma_1S_G.getValV();
		double lower_2S = Mass_Mean_2S.getValV()-sf*Mass_Sigma_2S_G.getValV();
		double upper_2S = Mass_Mean_2S.getValV()+sf*Mass_Sigma_2S_G.getValV();

		//Mass_Mean_1S.setConstant(kTRUE);
		//Mass_Sigma_1S_G.setConstant(kTRUE);

		m->setRange("SB1",2.6,lower_1S);
		m->setRange("SB2",upper_1S,lower_2S);
		m->setRange("SB3",upper_2S,4.2);

		//m_result = PDF_B->fitTo(*data,Range("SB1,SB2,SB3"),NumCPU(cores),Strategy(2),Save(kTRUE),SumW2Error(kTRUE),Timer(kTRUE),Offset(kTRUE));
		//RooAbsReal* nll = PDF_B->createNLL(*BinnedDataSample,Range("SB1,SB2,SB3"),NumCPU(cores),Offset(1));

		RooAbsReal* nll = PDF_B->createNLL(*BinnedDataSample,Range("SB1,SB2,SB3"),Offset(1));
		//RooAbsReal* nll = PDF_B->createNLL(*data,Range("SB1,SB2,SB3"),Offset(1));

		RooMinimizer min(*nll);
		min.setStrategy(2);

		//min.optimizeConst(kTRUE);
		min.minimize("Minuit2","Migrad");
		min.hesse();
		min.save()->Print("v");


		//PDF_B->fitTo(*BinnedDataSample, Range("SB1,SB2,SB3"), Save(),NumCPU(8));
		//PDF_B->fitTo(*data, Range("SB1,SB2,SB3"), Save(), NumCPU(cores));
		//PDF_B->fitTo(*BinnedDataSample, Range("SB1,SB2"), Save(), NumCPU(cores));
		//m_result = model->fitTo(*BinnedDataSample,Save(kTRUE),SumW2Error(kFALSE),Offset(kTRUE));
		//f_NP_B.setConstant(kTRUE);
		//f_NP_B_MisID.setConstant(kTRUE);
		////Mass_Mean_1S.setConstant(kTRUE);
		//Mass_Sigma_1S_G.setConstant(kTRUE);
		//Bkgd_Exp_MisID.setConstant(kTRUE);
		//Bkgd_Exp_MisID1.setConstant(kTRUE);
		//Bkgd_Exp_NonPrompt.setConstant(kTRUE);
		//Bkgd_Exp_Prompt.setConstant(kTRUE);
		//NP_Vato_Frac.setConstant(kTRUE);
		//Life_NP_1S_Short.setConstant(kTRUE);
		//Life_NP_1S      .setConstant(kTRUE);
		//Life_NP_2S      .setConstant(kTRUE);
		//Life_NP_B1      .setConstant(kTRUE);
		//Life_NP_B11     .setConstant(kTRUE);
		//Life_NP_MisID   .setConstant(kTRUE);

		//f_NP_B_MisID.setRange(f_NP_B_MisID.getValV()-0.6*fabs(f_NP_B_MisID.getValV()),f_NP_B_MisID.getValV()+0.6*fabs(f_NP_B_MisID.getValV()));
		//f_NP_B.setRange(f_NP_B.getValV()-0.6*fabs(f_NP_B.getValV()),f_NP_B.getValV()+0.6*fabs(f_NP_B.getValV()));
		//Bkgd_Exp_MisID.setRange(Bkgd_Exp_MisID.getValV()-0.5*fabs(Bkgd_Exp_MisID.getValV()),Bkgd_Exp_MisID.getValV()+0.5*fabs(Bkgd_Exp_MisID.getValV()));
		//Bkgd_Exp_MisID.Print();
		//Bkgd_Exp_NonPrompt.setRange(Bkgd_Exp_NonPrompt.getValV()-0.5*fabs(Bkgd_Exp_NonPrompt.getValV()),Bkgd_Exp_NonPrompt.getValV()+0.5*fabs(Bkgd_Exp_NonPrompt.getValV()));
		//Bkgd_Exp_Prompt.setRange(Bkgd_Exp_Prompt.getValV()-0.7*fabs(Bkgd_Exp_Prompt.getValV()),Bkgd_Exp_Prompt.getValV()+0.7*fabs(Bkgd_Exp_Prompt.getValV()));
	}

	//if(sys>12 && sys<17 && pt<140){
		//TIterator* iter = params->createIterator() ;
		//RooRealVar *parg = 0;
		//while((parg=(RooRealVar*)iter->Next())) {
			//parg->setConstant(kTRUE);
		//}
	//}
		//f_NP_1S.setConstant(kFALSE);
		//f_NP_1S.setRange(f_NP_1S.getValV()*0.8,f_NP_1S.getValV()*1.2);
		//if(pt<140){
			//f_NP_2S.setConstant(kFALSE);
			//f_NP_2S.setRange(f_NP_2S.getValV()*0.8,f_NP_2S.getValV()*1.2);
		//}
		//f_NP_B.setConstant(kFALSE);
		//f_NP_B.setRange(f_NP_B.getValV()*0.8,f_NP_B.getValV()*1.2);
//
		//f_NP_B_MisID.setConstant(kFALSE);
		//f_NP_B_MisID.setRange(f_NP_B_MisID.getValV()*0.8,f_NP_B_MisID.getValV()*1.2);
//
		//N_1S.setConstant(kFALSE);
		//N_1S.setRange(N_1S.getValV()*0.8,N_1S.getValV()*1.2);
//
		//N_2S.setConstant(kFALSE);
		//N_2S.setRange(N_2S.getValV()*0.8,N_2S.getValV()*1.2);
//
		//N_B.setConstant(kFALSE);
		//N_B.setRange(N_B.getValV()*0.8,N_B.getValV()*1.2);

	//Systematics
	switch(sys){
		case 0:
			break;
		case 1:
			Life_NP_1S.setVal(1.32);
			Life_NP_1S.setConstant(kTRUE);
			break;
		case 2:
			Life_NP_1S.setVal(1.08);
			Life_NP_1S.setConstant(kTRUE);
			break;
		case 3:
			Mass_CB_SigmaScale.setConstant(kFALSE);
			//Mass_CB_SigmaScale.setRange(Mass_CB_SigmaScale.getValV()*0.9,Mass_CB_SigmaScale.getValV()*1.1);
			break;
		case 4:
			Mass_CBa.setConstant(kFALSE);
			break;
		case 5:
			Mass_CBn.setConstant(kFALSE);
			break;
		case 6:
			Mass_SigmaScal_2S.setConstant(kFALSE);
			break;
		case 7:
			Tau_ResoFrac2.setVal(1);
			Tau_ResoFrac2.setConstant(kTRUE);
			break;
		case 8:
			rho_1S->setVal(0.36);
			rho_1S->setConstant(kTRUE);
			break;
		case 9:
			rho_1S->setVal(0.30);
			rho_1S->setConstant(kTRUE);
			break;
		case 13:
			N_2S_SF.setVal(0.08);
			N_2S_SF.setConstant(kTRUE);
			break;
		case 14:
			N_2S_SF.setVal(0.04);
			N_2S_SF.setConstant(kTRUE);
			break;
		case 15:
			f_NP_2S.setVal(0.8);
			f_NP_2S.setConstant(kTRUE);
			break;
		case 16:
			f_NP_2S.setVal(0.6);
			f_NP_2S.setConstant(kTRUE);
			break;
	}

	//Number of parameters
	TIterator* iter = params->createIterator() ;
	RooRealVar *parg = 0;
	while((parg=(RooRealVar*)iter->Next())) {
		if(!parg->getAttribute("Constant"))
			nparams++;
	}

	//RooAbsReal* nll = model->createNLL(*BinnedDataSample,NumCPU(cores),Offset(1));


	//RooMinimizer min(*nll);

	//min.optimizeConst(kTRUE);
	//min.setOffsetting(kFALSE);
	//min.setStrategy(2);

	//min.minimize("Minuit2","Migrad");
	//min.minos();
	//min.hesse();
	//min.minos(*params);
	//m_result = min.save();

	//m_result = model->fitTo(*BinnedDataSample,NumCPU(8),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE));
	//m_result = model->fitTo(*BinnedDataSample,NumCPU(8),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE),Minimizer("Minuit2","migrad"));

	if(pt>200)
		m_result = model->fitTo(*data,NumCPU(cores),Strategy(2),Save(kTRUE),SumW2Error(kTRUE),Timer(kTRUE),Minos(RooArgSet(f_NP_1S, f_NP_2S, N_1S, N_2S)));
	else
		m_result = model->fitTo(*data,NumCPU(cores),Strategy(2),Save(kTRUE),SumW2Error(kTRUE),Timer(kTRUE));
	//m_result = model->fitTo(*data,NumCPU(cores),Strategy(2),Save(kTRUE),SumW2Error(kTRUE),Timer(kTRUE),Minos(RooArgSet(f_NP_1S, f_NP_2S, N_1S, N_2S)));
	//m_result = model->fitTo(*data,Strategy(0),Save(kTRUE),SumW2Error(kTRUE),Timer(kTRUE));

	//m_result = model->fitTo(*BinnedDataSample,NumCPU(cores),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE));

	//m_result = model->fitTo(*BinnedDataSample,NumCPU(cores),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE),Minimizer("Minuit2","hesse"));
	//m_result = model->fitTo(*BinnedDataSample,NumCPU(cores),Optimize(0),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE));
	//m_result = model->chi2FitTo(*BinnedDataSample,NumCPU(cores),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE));
	////////////////////////////////////////////////////////
	/// Set RooPlots
	////////////////////////////////////////////////////////
	unsigned mBins = 64;
	unsigned tauBins = 80;
	if(pt>140){
		mBins = 32;
		tauBins = 40;
	}

	m_mframe = m->frame(Bins(mBins), Title(" "));
	data ->plotOn(m_mframe,Name("m_data"), Invisible());
	model->plotOn(m_mframe,Name("m_model"), Invisible());
	//model->plotOn(m_mframe,Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)), FillColor(kBlue+2),LineColor(kBlue+2),DrawOption("F"),FillStyle(3345),Precision(1e-5));
	//model->plotOn(m_mframe,Components(RooArgSet(*PDF_B,*PDF_NP_1S,*PDF_NP_2S)),FillColor(kWhite),LineColor(kRed+2),DrawOption("CF"),Precision(1e-5));
	//model->plotOn(m_mframe,Components(RooArgSet(*PDF_B,*PDF_NP_1S,*PDF_NP_2S)),FillColor(kRed+2),LineColor(kRed+2),DrawOption("F"),FillStyle(3344),Precision(1e-5));
	//model->plotOn(m_mframe,Components(RooArgSet(*PDF_B,*PDF_NP_1S,*PDF_NP_2S)),LineColor(kRed+2),Precision(1e-5));
	//model->plotOn(m_mframe,Components(*PDF_B),      FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
	//model->plotOn(m_mframe,Components(*PDF_B),      LineColor(kGray+2),Precision(1e-5));
	//model->plotOn(m_mframe,Components(argus),LineColor(kRed),LineStyle(kDashed)) ;
	//model->plotOn(m_mframe,Components(chebyshev),LineStyle(kDashed)) ;
	model->plotOn(m_mframe,Components(RooArgSet(*PDF_P_B1)),LineColor(kGreen+1),LineStyle(kDashed),Precision(1e-5));
	model->plotOn(m_mframe,Components(RooArgSet(*PDF_NP_B)),LineColor(kCyan+2),LineStyle(kDashed),Precision(1e-5));
	model->plotOn(m_mframe,Components(RooArgSet(*PDF_P_1S,*PDF_P_2S)),LineColor(kMagenta),LineStyle(kDashed),Precision(1e-5));
	model->plotOn(m_mframe,Components(RooArgSet(*PDF_NP_1S,*PDF_NP_2S)),LineColor(kRed+2),LineStyle(kDashed),Precision(1e-5));
	data ->plotOn(m_mframe,MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
	model->plotOn(m_mframe, LineColor(kBlue),Precision(1e-5));
	//Copy for magnified plot
	m_mframeZ = (RooPlot*)m_mframe->Clone("ZoomPsi");
	//data->statOn(m_mframe,Layout(0.55,0.95,0.95));
	//model->paramOn(m_mframe,Layout(0.47));
	//m_mframe->getAttText()->SetTextSize(0.025);
	m_ReducedChi2_m = m_mframe->chiSquare("m_model","m_data",nparams);
	TText* txt_chi2_m = new TText(0.4,0.96,Form("Chi2/ndof : %4f", m_ReducedChi2_m)) ;
	txt_chi2_m->SetNDC();
	txt_chi2_m->SetTextSize(0.04) ;
	txt_chi2_m->SetTextColor(kRed) ;
	m_mframe->addObject(txt_chi2_m) ;

	RooHist* m_hpull = m_mframe->pullHist();
	m_pullframe = m->frame(Bins(mBins), Title("Dimuon mass pull"));
	m_pullframe->addPlotable((RooPlotable *)m_hpull,"P");

	//t_mframe = tau->frame(Bins(1650), Title("proper time"));
	t_mframe = tau->frame(Bins(tauBins), Title(" "));
	t_mframe->SetAxisRange(-1,11);
	data ->plotOn(t_mframe,Name("t_data"),Invisible());
	model->plotOn(t_mframe,Name("t_model"),Invisible());
	//model->plotOn(t_mframe,Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)), FillColor(kGray),LineColor(kGray+2),DrawOption("CF"),Precision(1e-5));
	//// Prompt Signal
	//model->plotOn(t_mframe,Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)),FillColor(kWhite),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
	//model->plotOn(t_mframe,Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)),FillColor(kBlue+2),LineColor(kBlue+2),DrawOption("F"),FillStyle(3345),Precision(1e-5));
	//model->plotOn(t_mframe,Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)),LineColor(kBlue+2),Precision(1e-5));
	//// Non-Prompt Signal
	//model->plotOn(t_mframe,Components(RooArgSet(*PDF_NP_1S,*PDF_NP_2S)),FillColor(kWhite),LineColor(kRed+2),DrawOption("CF"),Precision(1e-5));
	//model->plotOn(t_mframe,Components(RooArgSet(*PDF_NP_1S,*PDF_NP_2S)),FillColor(kRed+2),LineColor(kRed+2),DrawOption("F"),FillStyle(3344),Precision(1e-5));
	//model->plotOn(t_mframe,Components(RooArgSet(*PDF_NP_1S,*PDF_NP_2S)),LineColor(kRed+2),Precision(1e-5));
	//
	//model->plotOn(t_mframe,Components(RooArgSet(mPDF_Tau_NP_1S_Long)),FillColor(kWhite),LineColor(kOrange+10),DrawOption("CF"),Precision(1e-5));
	//model->plotOn(t_mframe,Components(RooArgSet(mPDF_Tau_NP_1S_Long)),FillColor(kOrange+10),LineColor(kOrange+10),DrawOption("F"),FillStyle(3344),Precision(1e-5));
	//model->plotOn(t_mframe,Components(RooArgSet(mPDF_Tau_NP_1S_Long)),LineColor(kOrange+10),Precision(1e-5));
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_P_B1)),LineColor(kGreen+1),LineStyle(kDashed),Precision(1e-5));
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_NP_B)),LineColor(kCyan+2),LineStyle(kDashed),Precision(1e-5));
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_P_1S,*PDF_P_2S)),LineColor(kMagenta),LineStyle(kDashed),Precision(1e-5));
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_NP_1S,*PDF_NP_2S)),LineColor(kRed+2),LineStyle(kDashed),Precision(1e-5));
	// Data and total fit
	data ->plotOn(t_mframe,MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
	model->plotOn(t_mframe,LineColor(kBlue),Precision(1e-5));
	//model->plotOn(t_mframe,LineColor(1),Precision(1e-5),ProjWData(*m,*BinnedDataSample));
	//data->statOn(t_mframe,Layout(0.55,0.95,0.95));
	//model->paramOn(t_mframe,Layout(0.47,0.95));
	//t_mframe->getAttText()->SetTextSize(0.025);
	m_ReducedChi2_tau = t_mframe->chiSquare("t_model","t_data",nparams);
	TText* txt_chi2_tau = new TText(0.4,0.96,Form("Chi2/ndof : %4f", m_ReducedChi2_tau)) ;
	txt_chi2_tau->SetNDC();
	txt_chi2_tau->SetTextSize(0.04) ;
	txt_chi2_tau->SetTextColor(kRed) ;
	t_mframe->addObject(txt_chi2_tau) ;

	//t_mframe_pull = tau->frame(Bins(1650), Title("proper time pull"));
	RooHist* t_hpull = t_mframe->pullHist(0,0,kTRUE);
	t_pullframe = tau->frame(Bins(tauBins), Title("proper time pull"));
	t_pullframe->SetAxisRange(-1,11);
	t_pullframe->addPlotable((RooPlotable *)t_hpull,"P");

	//2D plots
	/*
	   double low=0, high=0;
	   data->getRange(*tau,low,high);
	   cout<<low<<" "<<high<<endl;
	   RooRealVar* xvar = (RooRealVar*) data->get()->find("tau") ;
	   cout<<xvar->getMin()<<" "<<xvar->getMax()<<endl;;
	   */
	//hh_data = data->createHistogram("m,tau",64,600);
	hh_data = data->createHistogram("hh_data",*m,Binning(64,2.6,4.2),YVar(*tau,Binning(160,-1.0,15.)));
	hh_pdf = model->createHistogram("m,tau",64,160);
	//hf_pdf = model->asTF(RooArgList(*m,*tau),params);

	m_ReducedChi2 = m_mframe->chiSquare("m_model","m_data",nparams);

	////////////////////////////////////////////////////////
	/// Save Params for next bin
	///////////////////////////////////////////////////////
	RooArgSet* tmp = (RooArgSet*)params->snapshot(kTRUE);
	//savedParams = (RooArgSet*)tmp->clone("saved");
	savedParams->addOwned(*tmp);
	*savedParams = *tmp;
	std::cout<<tmp<<" "<<savedParams<<" "<<*tmp<<" "<<*savedParams<<std::endl;
	tmp->Print("v");

	//std::ofstream outfile(outF->Data(),std::ios_base::app);
	//outfile<<"y: "<<theHelper.y_index<<" pt: "<<theHelper.pt_index<<endl;
	if(!drawOnly){
		//std::cout.precision(15);
		//savedParams->writeToFile(outF->Data());
		std::ofstream os(outF->Data(),std::ios_base::app);
		//std::ofstream os(outF->Data());
		os.precision(15);
		savedParams->writeToStream(os,0);
		m_result->printValue(os);
		os.close();
		//outfile.close();
	}
	//if(m_ReducedChi2_m > 7 || m_ReducedChi2_tau > 7) savedParams->removeAll();
	//savedParams->removeAll(); //Don't save parameters for next iter
	savedParams->Print("v");



	if(!drawOnly){
		m_val.Inclusive1S 	  = N_1S.getVal();
		m_val.Inclusive1S_Err = N_1S.getPropagatedError(*m_result);
		m_val.Inclusive2S     = N_2S.getVal();
		m_val.Inclusive2S_Err = N_2S.getPropagatedError(*m_result);
		m_val.Prompt1S = N_1S_P.getVal();
		m_val.Prompt2S = N_2S_P.getVal();
		m_val.Prompt1S_Err = N_1S_P.getPropagatedError(*m_result);
		m_val.Prompt2S_Err = N_2S_P.getPropagatedError(*m_result);
		m_val.NonPrompt1S = N_1S_NP.getVal();
		m_val.NonPrompt2S = N_2S_NP.getVal();
		m_val.NonPrompt1S_Err = N_1S_NP.getPropagatedError(*m_result);
		m_val.NonPrompt2S_Err = N_2S_NP.getPropagatedError(*m_result);
		m_val.NPFraction1S       = f_NP_1S.getVal();
		m_val.NPFraction2S       = f_NP_2S.getVal();
		m_val.NPFraction1S_Err   = f_NP_1S.getPropagatedError(*m_result);
		m_val.NPFraction2S_Err   = f_NP_2S.getPropagatedError(*m_result);
		//m_val.PromptRatio    = Ratio_P.getVal();
		//m_val.NonPromptRatio = Ratio_NP.getVal();
		//m_val.PromptRatio_Err    = Ratio_P.getPropagatedError(*m_result);
		//m_val.NonPromptRatio_Err = Ratio_NP.getPropagatedError(*m_result);

		m_val.MassSigma1S = TMath::Sqrt(Mass_Sigma_1S_CB.getVal()*Mass_Sigma_1S_CB.getVal());
		m_val.MassSigma2S = TMath::Sqrt(Mass_Sigma_2S_CB.getVal()*Mass_Sigma_2S_CB.getVal());
		m_val.TauSigma1   = Tau_Sigma1.getVal();
		m_val.TauSigma2   = Tau_Sigma2.getVal();

		m_tau_sigma = Tau_Sigma1.getVal();
		m_life_1s = Life_NP_1S.getVal();

		m_result->Print("v");
	}

	//m_frac.fsignal      = fsignal.getVal();
	//m_frac.fsigPrompt   = fsigPrompt.getVal();
	//m_frac.fracPPsi2S   = fPPsi2S.getVal();
	//m_frac.fracNPPsi2S  = fNPPsi2S.getVal();
	//m_frac.fbkg1        = f_bkg_b1.getVal();
	//m_frac.fbkg2        = f_bkg_b2.getVal();
	//m_fsigma = fSigma.getVal();
	////////////////////////////////////////////////////////
	/// Set Additional RooPlots
	////////////////////////////////////////////////////////
	if (m_extraPlots) {
		//m_mframe_RangePsi   = m->frame(Bins(75), Title("Dimuon mass"));
		m_mframe_RangeP     = m->frame(Bins(64), Title("Dimuon mass P"));
		m_mframe_RangeNP    = m->frame(Bins(64), Title("Dimuon mass NP"));
		m_mframe_RangeNP1    = m->frame(Bins(64), Title("Dimuon mass NP1"));
		m_mframe_RangeMisID = m->frame(Bins(64), Title("Dimuon mass MisID"));
		t_mframe_RangePreJpsi  = tau->frame(Bins(800), Title("proper time PreJpsi"));
		t_mframe_RangeJpsi  = tau->frame(Bins(800), Title("proper time Jpsi"));
		t_mframe_RangePrePsi   = tau->frame(Bins(800), Title("proper time PrePsi"));
		t_mframe_RangePsi   = tau->frame(Bins(800), Title("proper time Psi"));
		t_mframe_RangePostPsi   = tau->frame(Bins(800), Title("proper time PostPsi"));

		/// Jpsi lifetime fit projection
		m->  setRange("MassRangePreJpsi",2.6,2.85);
		RooDataSet* data_RangePreJpsi = (RooDataSet*)data->reduce(Cut("m>2.6 && m<2.85"));
		data_RangePreJpsi->plotOn(t_mframe_RangePreJpsi, Invisible());
		model       ->plotOn(t_mframe_RangePreJpsi, NormRange("MassRangePreJpsi"),ProjectionRange("MassRangePreJpsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangePreJpsi, NormRange("MassRangePreJpsi"),ProjectionRange("MassRangePreJpsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangeJpsi, NormRange("MassRangePreJpsi"),ProjectionRange("MassRangePreJpsi"),Components(RooArgSet(*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangePreJpsi->plotOn(t_mframe_RangePreJpsi, MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
		model         ->plotOn(t_mframe_RangePreJpsi, NormRange("MassRangePreJpsi"),ProjectionRange("MassRangePreJpsi"), LineColor(kRed-3),Precision(1e-5));
		model->paramOn(t_mframe_RangePreJpsi,Layout(0.47,0.95));
		t_mframe_RangePreJpsi->getAttText()->SetTextSize(0.025);
		data_RangePreJpsi->statOn(t_mframe_RangePreJpsi,Layout(0.55,0.95,0.95));

		delete data_RangePreJpsi;

		m->  setRange("MassRangeJpsi",2.85,3.35);
		//tau->setRange("MassRangeJpsi",-5,15);
		RooDataSet* data_RangeJpsi = (RooDataSet*)data->reduce(Cut("m>2.85 && m<3.35"));
		data_RangeJpsi->plotOn(t_mframe_RangeJpsi, Invisible());
		model	      ->plotOn(t_mframe_RangeJpsi, ProjectionRange("MassRangeJpsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model	      ->plotOn(t_mframe_RangeJpsi, ProjectionRange("MassRangeJpsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model	      ->plotOn(t_mframe_RangeJpsi,ProjectionRange("MassRangeJpsi"),Components(RooArgSet(*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeJpsi->plotOn(t_mframe_RangeJpsi, MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
		model         ->plotOn(t_mframe_RangeJpsi, NormRange("MassRangeJpsi"),ProjectionRange("MassRangeJpsi"), LineColor(kRed-3),Precision(1e-5));
		//model         ->plotOn(t_mframe_RangeJpsi, ProjectionRange("MassRangeJpsi"), LineColor(kRed-3),Precision(1e-5));
		data_RangeJpsi->statOn(t_mframe_RangeJpsi,Layout(0.55,0.95,0.95));

		delete data_RangeJpsi;

		m->  setRange("MassRangePrePsi",3.35,3.55);
		RooDataSet* data_RangePrePsi = (RooDataSet*)data->reduce(Cut("m>3.35 && m<3.55"));
		data_RangePrePsi->plotOn(t_mframe_RangePrePsi, Invisible());
		model       ->plotOn(t_mframe_RangePrePsi, NormRange("MassRangePrePsi"),ProjectionRange("MassRangePrePsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangePrePsi, NormRange("MassRangePrePsi"),ProjectionRange("MassRangePrePsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangePsi, NormRange("MassRangePrePsi"),ProjectionRange("MassRangePrePsi"),Components(RooArgSet(*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangePrePsi->plotOn(t_mframe_RangePrePsi, MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
		model         ->plotOn(t_mframe_RangePrePsi, NormRange("MassRangePrePsi"),ProjectionRange("MassRangePrePsi"), LineColor(kRed-3),Precision(1e-5));
		data_RangePrePsi->statOn(t_mframe_RangePrePsi,Layout(0.55,0.95,0.95));

		delete data_RangePrePsi;

		m->  setRange("MassRangePsi",3.55,3.85);
		/// Psi(2S) lifetime fit projection
		RooDataSet* data_RangePsi = (RooDataSet*)data->reduce(Cut("m>3.55 && m<3.85"));
		data_RangePsi->plotOn(t_mframe_RangePsi, Invisible());
		model	        ->plotOn(t_mframe_RangePsi, NormRange("MassRangePsi"),ProjectionRange("MassRangePsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		//model	        ->plotOn(t_mframe_RangePsi, ProjectionRange("MassRangePsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model	        ->plotOn(t_mframe_RangePsi, NormRange("MassRangePsi"),ProjectionRange("MassRangePsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		//model	        ->plotOn(t_mframe_RangePsi, ProjectionRange("MassRangePsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model	        ->plotOn(t_mframe_RangePsi, NormRange("MassRangePsi"),ProjectionRange("MassRangePsi"),Components(RooArgSet(*PDF_NP_1S,*PDF_NP_2S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		//model	        ->plotOn(t_mframe_RangePsi,ProjectionRange("MassRangePsi"),Components(RooArgSet(*PDF_NP_1S,*PDF_NP_2S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangePsi->plotOn(t_mframe_RangePsi, CutRange("MassRangePsi"), MarkerSize(1.0),XErrorSize(0), DataError(RooAbsData::SumW2));
		model           ->plotOn(t_mframe_RangePsi, NormRange("MassRangePsi"),ProjectionRange("MassRangePsi"), LineColor(kRed-3),Precision(1e-5));
		//model           ->plotOn(t_mframe_RangePsi,ProjectionRange("MassRangePsi"), LineColor(kRed-3),Precision(1e-5));
		data_RangePsi->statOn(t_mframe_RangePsi, Layout(0.55,0.95,0.95));

		delete data_RangePsi;

		m->  setRange("MassRangePostPsi",3.85,4.2);
		RooDataSet* data_RangePostPsi = (RooDataSet*)data->reduce(Cut("m>3.85 && m<4.2"));
		data_RangePostPsi->plotOn(t_mframe_RangePostPsi, Invisible());
		model       ->plotOn(t_mframe_RangePostPsi, NormRange("MassRangePostPsi"),ProjectionRange("MassRangePostPsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangePostPsi, NormRange("MassRangePostPsi"),ProjectionRange("MassRangePostPsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S,*PDF_P_2S,*PDF_NP_2S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangePostPsi, NormRange("MassRangePostPsi"),ProjectionRange("MassRangePostPsi"),Components(RooArgSet(*PDF_NP_1S,*PDF_NP_2S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangePostPsi->plotOn(t_mframe_RangePostPsi, MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
		model         ->plotOn(t_mframe_RangePostPsi, NormRange("MassRangePostPsi"),ProjectionRange("MassRangePostPsi"), LineColor(kRed-3),Precision(1e-5));
		data_RangePostPsi->statOn(t_mframe_RangePostPsi,Layout(0.55,0.95,0.95));

		delete data_RangePostPsi;

		/// Prompt evt mass projection
		/// mis-ID evt mass projection
		//m->  setRange("TauRangeMisID", 2.6, 4.2);
		tau->setRange("TauRangeMisID",-1.0,-0.6);
		RooDataSet* data_RangeMisID = (RooDataSet*)data->reduce(Cut("tau>-1.0 && tau<-0.6"));
		data_RangeMisID->plotOn(m_mframe_RangeMisID, Invisible());
		model          ->plotOn(m_mframe_RangeMisID, NormRange("TauRangeMisID"), ProjectionRange("TauRangeMisID"), Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)),FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model          ->plotOn(m_mframe_RangeMisID, NormRange("TauRangeMisID"), ProjectionRange("TauRangeMisID"), Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model          ->plotOn(m_mframe_RangeMisID, NormRange("TauRangeMisID"), ProjectionRange("TauRangeMisID"), Components(*PDF_B), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeMisID->plotOn(m_mframe_RangeMisID, MarkerSize(1.0),XErrorSize(0), DataError(RooAbsData::SumW2));
		model          ->plotOn(m_mframe_RangeMisID, NormRange("TauRangeMisID"), ProjectionRange("TauRangeMisID"), LineColor(kRed-3),Precision(1e-5));
		data_RangeMisID->statOn(m_mframe_RangeMisID,Layout(0.55,0.95,0.95));

		delete data_RangeMisID;

		//m->  setRange("TauRangeP", 2.6,4.2);
		tau->setRange("TauRangeP",-0.6,0.6);
		RooDataSet* data_RangeP = (RooDataSet*)data->reduce(Cut("tau>-0.6 && tau<0.6"));
		data_RangeP->plotOn(m_mframe_RangeP, Invisible());
		model      ->plotOn(m_mframe_RangeP, NormRange("TauRangeP"), ProjectionRange("TauRangeP"), Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)),FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model      ->plotOn(m_mframe_RangeP, NormRange("TauRangeP"), ProjectionRange("TauRangeP"), Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model      ->plotOn(m_mframe_RangeP, NormRange("TauRangeP"), ProjectionRange("TauRangeP"), Components(*PDF_B), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeP->plotOn(m_mframe_RangeP, MarkerSize(1.0), XErrorSize(0), DataError(RooAbsData::SumW2));
		model      ->plotOn(m_mframe_RangeP, NormRange("TauRangeP"), ProjectionRange("TauRangeP"), LineColor(kRed-3),Precision(1e-5));
		data_RangeP->statOn(m_mframe_RangeP,Layout(0.55,0.95,0.95));

		delete data_RangeP;

		/// Non-Prompt evt mass projection
		//m  ->setRange("TauRangeNP", 2.6, 4.2);
		tau->setRange("TauRangeNP", 0.6, 3.0);
		RooDataSet* data_RangeNP = (RooDataSet*)data->reduce(Cut("tau>0.6 && tau<3.0"));
		data_RangeNP->plotOn(m_mframe_RangeNP, Invisible());
		model       ->plotOn(m_mframe_RangeNP, NormRange("TauRangeNP"), ProjectionRange("TauRangeNP"), Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)),FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(m_mframe_RangeNP, NormRange("TauRangeNP"), ProjectionRange("TauRangeNP"), Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(m_mframe_RangeNP, NormRange("TauRangeNP"), ProjectionRange("TauRangeNP"), Components(*PDF_B), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeNP->plotOn(m_mframe_RangeNP, MarkerSize(1.0), XErrorSize(0), DataError(RooAbsData::SumW2));
		model       ->plotOn(m_mframe_RangeNP, NormRange("TauRangeNP"), ProjectionRange("TauRangeNP"), LineColor(kRed-3),Precision(1e-5));
		data_RangeNP->statOn(m_mframe_RangeNP,Layout(0.55,0.95,0.95));

		delete data_RangeNP;

		tau->setRange("TauRangeNP1", 3.0, 15.);
		RooDataSet* data_RangeNP1 = (RooDataSet*)data->reduce(Cut("tau>3.0"));
		data_RangeNP1->plotOn(m_mframe_RangeNP1, Invisible());
		model       ->plotOn(m_mframe_RangeNP1, NormRange("TauRangeNP1"), ProjectionRange("TauRangeNP1"), Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)),FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(m_mframe_RangeNP1, NormRange("TauRangeNP1"), ProjectionRange("TauRangeNP1"), Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(m_mframe_RangeNP1, NormRange("TauRangeNP1"), ProjectionRange("TauRangeNP1"), Components(*PDF_B), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeNP1->plotOn(m_mframe_RangeNP1, MarkerSize(1.0), XErrorSize(0), DataError(RooAbsData::SumW2));
		model       ->plotOn(m_mframe_RangeNP1, NormRange("TauRangeNP1"), ProjectionRange("TauRangeNP1"), LineColor(kRed-3),Precision(1e-5));
		data_RangeNP1->statOn(m_mframe_RangeNP1,Layout(0.55,0.95,0.95));

		delete data_RangeNP1;

	}

	if(!drawOnly)
		m_result->Print("v");

	if (Tau_Reso1) { delete Tau_Reso1; Tau_Reso1 = 0; }
	if (Tau_Reso2) { delete Tau_Reso2; Tau_Reso2 = 0; }
	if (Tau_Reso3) { delete Tau_Reso3; Tau_Reso3 = 0; }
	if (Tau_Reso4) { delete Tau_Reso4; Tau_Reso4 = 0; }

	if (mPDF_Tau_NP_1S_0) { delete mPDF_Tau_NP_1S_0; mPDF_Tau_NP_1S_0 = 0; }
	if (mPDF_Tau_NP_2S_0) { delete mPDF_Tau_NP_2S_0; mPDF_Tau_NP_2S_0 = 0; }
	if (mPDF_Tau_NP_1S_1) { delete mPDF_Tau_NP_1S_1; mPDF_Tau_NP_1S_1 = 0; }
	if (mPDF_Tau_NP_2S_1) { delete mPDF_Tau_NP_2S_1; mPDF_Tau_NP_2S_1 = 0; }
	if (mPDF_Tau_NP_1S_2) { delete mPDF_Tau_NP_1S_2; mPDF_Tau_NP_1S_2 = 0; }
	if (mPDF_Tau_NP_2S_2) { delete mPDF_Tau_NP_2S_2; mPDF_Tau_NP_2S_2 = 0; }

	//if (mPDF_Tau_NP_1S) { delete mPDF_Tau_NP_1S; mPDF_Tau_NP_1S = 0; }
	//if (mPDF_Tau_NP_2S) { delete mPDF_Tau_NP_2S; mPDF_Tau_NP_2S = 0; }

	//if (mPDF_Mass_Bkgd_NonPrompt_Exp) { delete mPDF_Mass_Bkgd_NonPrompt_Exp; mPDF_Mass_Bkgd_NonPrompt_Exp = 0; }
	if (chebyshev) { delete chebyshev; chebyshev = 0; }
	if (PDF_P_1S_1)  { delete PDF_P_1S_1;  PDF_P_1S_1 = 0;  }
	if (PDF_P_1S_2)  { delete PDF_P_1S_2;  PDF_P_1S_2 = 0;  }
	if (PDF_P_1S_3)  { delete PDF_P_1S_3;  PDF_P_1S_3 = 0;  }
	if (w1)  { delete w1;  w1 = 0;  }

	//if (PDF_P_1S_tmp)  { delete PDF_P_1S_tmp;  PDF_P_1S_tmp = 0;  }

	if (PDF_P_1S)  { delete PDF_P_1S;  PDF_P_1S = 0;  }
	if (PDF_P_2S)  { delete PDF_P_2S;  PDF_P_2S = 0;  }
	if (PDF_NP_1S) { delete PDF_NP_1S; PDF_NP_1S = 0; }
	if (PDF_NP_2S) { delete PDF_NP_2S; PDF_NP_2S = 0; }
	if (PDF_P_B1)  { delete PDF_P_B1;  PDF_P_B1 = 0;  }
	if (PDF_NP_B1) { delete PDF_NP_B1; PDF_NP_B1 = 0; }
	if (PDF_NP_B2) { delete PDF_NP_B2; PDF_NP_B2 = 0; }
	if (PDF_NP_B21) { delete PDF_NP_B21; PDF_NP_B21 = 0; }
	if (PDF_NP_B)  { delete PDF_NP_B;  PDF_NP_B = 0;  }
	if (PDF_1S)    { delete PDF_1S;    PDF_1S = 0;    }
	if (PDF_2S)    { delete PDF_2S;    PDF_2S = 0;    }
	if (PDF_B)     { delete PDF_B;     PDF_B = 0;     }
	if (ePDF_1S)   { delete ePDF_1S;   ePDF_1S = 0;   }
	if (ePDF_2S)   { delete ePDF_2S;   ePDF_2S = 0;   }
	if (ePDF_B)    { delete ePDF_B;    ePDF_B = 0;    }
	if (N_2S_fixed){ delete N_2S_fixed;    N_2S_fixed = 0;    }
	if (M1D_ePDF_1S)   { delete M1D_ePDF_1S;   M1D_ePDF_1S = 0;   }
	if (M1D_ePDF_2S)   { delete M1D_ePDF_2S;   M1D_ePDF_2S = 0;   }
	if (M1D_ePDF_B)    { delete M1D_ePDF_B;    M1D_ePDF_B = 0;    }
	if (M1D_model)     { delete M1D_model;     M1D_model = 0;     }
	if (model)     { delete model;     model = 0;     }
	if (BinnedDataSample) { delete BinnedDataSample; }

	return m_result;
}

RooFitResult* myCharmFitter::Nominal_Fit_JPsi(RooDataSet* data, RooArgSet* savedParams, TString* inputF, TString* outF, int life){
	////////////////////////////////////////////////////////
	// Time Signal
	////////////////////////////////////////////////////////
	RooRealVar    Tau_Mean("Tau_Mean","tau mean",0.0);
	RooRealVar    Tau_Mean1("Tau_Mean1","tau mean 1",0.0, -0.005,0.005);
	RooRealVar    Tau_Mean2("Tau_Mean2","tau mean 2",0.0, -0.01,0.01);
	RooRealVar    Tau_Mean3("Tau_Mean3","tau mean 2",0.0, -0.015,0.015);
	RooRealVar    Tau_Sigma1("Tau_Sigma1", "Gaussian reso sigma", 0.051, 0.005, 0.075);//0.06
	//RooRealVar    Tau_SigmaScal("Tau_SigmaScale","scaling between double Gaussian resolution",1.59,1.1,2.5);//1.83
	RooRealVar    Tau_SigmaScal("Tau_SigmaScale","scaling between double Gaussian resolution",2);//1.83
	RooFormulaVar Tau_Sigma2("Tau_Sigma2", "@0 * @1",RooArgList(Tau_Sigma1,Tau_SigmaScal));
	//RooRealVar    Tau_SigmaScal1("Tau_SigmaScale1","scaling between double Gaussian resolution",2.7,2.7,3.5);//2.4
	RooRealVar    Tau_SigmaScal1("Tau_SigmaScale1","scaling between double Gaussian resolution",3.0);//2.4
	RooFormulaVar Tau_Sigma3("Tau_Sigma3", "@0 * @1",RooArgList(Tau_Sigma1,Tau_SigmaScal1));
	//RooRealVar    Tau_SigmaScal2("Tau_SigmaScale2","scaling between double Gaussian resolution",3.9,3.9,6.0);//4.0
	RooRealVar    Tau_SigmaScal2("Tau_SigmaScale2","scaling between double Gaussian resolution",5.0);//4.0
	RooFormulaVar Tau_Sigma4("Tau_Sigma4", "@0 * @1",RooArgList(Tau_Sigma1,Tau_SigmaScal2));
	//RooRealVar    Tau_ResoFrac1("Tau_ResoFrac1","frac between double Gaussian resolution",0.8,0.78,0.82);//0.72
	RooRealVar    Tau_ResoFrac1("Tau_ResoFrac1","frac between double Gaussian resolution",0.8,0,1);//0.72
	//RooRealVar    Tau_ResoFrac2("Tau_ResoFrac2","frac between 3x Gaussian resolution",0.64,0,1);
	RooRealVar    Tau_ResoFrac2("Tau_ResoFrac2","frac between 3x Gaussian resolution",0.64,0,1);
	//RooRealVar    Tau_ResoFrac3("Tau_ResoFrac3","frac between 4x Gaussian resolution",0.5,0,1);
	RooRealVar    Tau_ResoFrac3("Tau_ResoFrac3","frac between 4x Gaussian resolution",1,0,1);
	// Lifetimes for b-decays and prompt (e.g. 0)
	RooRealVar Life_NP_1S("Life_NP_1S","",1.3,0.8,3.0);
	RooRealVar Life_P("Life_P","",0.0);
	//Tau_Sigma1.setConstant(kTRUE);
	//Tau_ResoFrac.setConstant(kTRUE);
	//Tau_ResoFrac1.setConstant(kTRUE);
	//Tau_ResoFrac2.setConstant(kTRUE);
	//Tau_ResoFrac3.setConstant(kTRUE);
	//Tau_SigmaScal.setConstant(kTRUE);
	//Tau_SigmaScal1.setConstant(kTRUE);
	//Tau_SigmaScal2.setConstant(kTRUE);
	Tau_Mean1.setConstant(kTRUE);
	Tau_Mean2.setConstant(kTRUE);
	Tau_Mean3.setConstant(kTRUE);

	if (m_fix_decay) {
		Tau_Sigma1.setVal(m_tau_sigma);
		Life_NP_1S.setVal(m_life_1s);
		Tau_Sigma1.setConstant(kTRUE);
		Life_NP_1S.setConstant(kTRUE);
	}

	RooRealVar Life_NP_1S_Short("Life_NP_1S_Short","",0.4,0.3,0.5);
	//RooRealVar Life_NP_1S_Medium("Life_NP_1S_Medium","",1.35,1.1,1.45);
	//RooRealVar Life_NP_1S_Long("Life_NP_1S_Long","",1.5,1.45,1.6);
	RooRealVar Life_NP_1S_Long("Life_NP_1S_Long","",1.3,1.0,2.0);
	//RooRealVar Life_NP_1S_LongScale("Life_NP_1S_LongScale","",2.0,1.0,3.0);
	Life_NP_1S_Short.setConstant(kTRUE);
	//Life_NP_1S_LongScale.setConstant(kTRUE);
	//RooFormulaVar Life_NP_1S_Long("Life_NP_1S_Long","Life_NP_1S_LongScale*Life_NP_1S_Short",RooArgSet(Life_NP_1S_LongScale,Life_NP_1S_Short));
	//RooRealVar f_Life_NP_1S_Short("f_Life_NP_1S_Short","",0.5,0.0,1.0);
	RooRealVar f_Life_NP_1S_Short("f_Life_NP_1S_Short","",0.5,-0.2,1.2);
	RooRealVar f_Life_NP_1S_Medium("f_Life_NP_1S_Medium","",0.5,0.0,1.0);

	RooGaussModel* Tau_Reso1 = new RooGaussModel("Tau_Reso1", "", *tau, Tau_Mean, Tau_Sigma1);
	//RooGaussModel* Tau_Reso2 = new RooGaussModel("Tau_Reso2", "", *tau, Tau_Mean, Tau_Sigma2);
	RooGaussModel* Tau_Reso2 = new RooGaussModel("Tau_Reso2", "", *tau, Tau_Mean1, Tau_Sigma2);
	RooGaussModel* Tau_Reso3 = new RooGaussModel("Tau_Reso3", "", *tau, Tau_Mean2, Tau_Sigma3);
	RooGaussModel* Tau_Reso4 = new RooGaussModel("Tau_Reso4", "", *tau, Tau_Mean3, Tau_Sigma4);
	//RooResolutionModel* Tau_Reso_2G = new RooAddModel("Tau_Reso_2G","double Gaussian Tau Reso",RooArgList(*Tau_Reso1,*Tau_Reso2),RooArgSet(Tau_ResoFrac1));
	mPDF_Tau_Reso = new RooAddModel("Tau_Reso_2G","double Gaussian Tau Reso",RooArgList(*Tau_Reso1,*Tau_Reso2),RooArgSet(Tau_ResoFrac1));
	//RooResolutionModel* Tau_Reso_3G = new RooAddModel("Tau_Reso_3G","triple Gaussian Tau Reso",RooArgList(*Tau_Reso_2G,*Tau_Reso3),RooArgSet(Tau_ResoFrac2));
	//mPDF_Tau_Reso = new RooAddModel("Tau_Reso_3G","triple Gaussian Tau Reso",RooArgList(*Tau_Reso_2G,*Tau_Reso3),RooArgSet(Tau_ResoFrac2));
	//mPDF_Tau_Reso = new RooAddModel("mPDF_Tau_Reso","4 Gaussian Tau Reso",RooArgList(*Tau_Reso_3G,*Tau_Reso4),RooArgSet(Tau_ResoFrac3));
	//RooRealVar    Tau_f1("Tau_f1","Tau_f1",0.4);
	//RooRealVar    Tau_f2("Tau_f2","Tau_f2",1.2);
	//RooRealVar    Tau_f3("Tau_f3","Tau_f3",1.2);
	//RooRealVar    Tau_f4("Tau_f4","Tau_f4",1.2);
	//mPDF_Tau_Reso = new RooAddModel("mPDF_Tau_Reso","triple Gaussian Tau Reso",RooArgList(*Tau_Reso1,*Tau_Reso2,*Tau_Reso3,*Tau_Reso4),RooArgSet(Tau_f1,Tau_f2,Tau_f3,Tau_f4));
	RooDecay mPDF_Tau_NP_1S_Short("mPDF_Tau_P_1S_Short","", *tau, Life_NP_1S_Short, *mPDF_Tau_Reso, RooDecay::SingleSided);
	//RooDecay mPDF_Tau_NP_1S_Medium("mPDF_Tau_P_1S_Medium","", *tau, Life_NP_1S_Medium, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooDecay mPDF_Tau_NP_1S_Long( "mPDF_Tau_P_1S_Long","",  *tau, Life_NP_1S_Long,  *mPDF_Tau_Reso, RooDecay::SingleSided);
	//f_Life_NP_1S_Short.setConstant(kTRUE);
	//RooAbsPdf* mPDF_Tau_NP_1S_doub = new RooAddPdf("mPDF_Tau_NP_1S_doub","",RooArgSet(mPDF_Tau_NP_1S_Short,mPDF_Tau_NP_1S_Medium),f_Life_NP_1S_Short); //Part of triple
	//mPDF_Tau_NP_1S = new RooAddPdf("mPDF_Tau_NP_1S","",RooArgSet(*mPDF_Tau_NP_1S_doub,mPDF_Tau_NP_1S_Long),f_Life_NP_1S_Medium); // Triple
	RooRealVar k("k","",0.5,0.2,0.8);

	RooRealVar f1("f1","",2.0,1.2,2.0);
	f1.setConstant(kTRUE);
	//RooRealVar f2("f2","",0.3,0,0.9);
	RooFormulaVar f2("f2","2.0-f1",RooArgSet(f1));
	RooRealVar f3("f3","",1.0);

	//f1.setConstant(kTRUE);
	//f2.setConstant(kTRUE);
	f3.setConstant(kTRUE);

	RooFormulaVar Life_NP_1S_1("Life_NP_1S_1","Life_NP_1S*(1-k*0.707)",RooArgList(Life_NP_1S,k));
	RooFormulaVar Life_NP_1S_2("Life_NP_1S_2","Life_NP_1S*(1+k*0.707)",RooArgList(Life_NP_1S,k));

	RooAbsPdf* mPDF_Tau_NP_1S_0 = new RooDecay("mPDF_Tau_NP_1S_0","", *tau, Life_NP_1S, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooAbsPdf* mPDF_Tau_NP_1S_1 = new RooDecay("mPDF_Tau_NP_1S_1","", *tau, Life_NP_1S_1, *mPDF_Tau_Reso, RooDecay::SingleSided);
	RooAbsPdf* mPDF_Tau_NP_1S_2 = new RooDecay("mPDF_Tau_NP_1S_2","", *tau, Life_NP_1S_2, *mPDF_Tau_Reso, RooDecay::SingleSided);

	mPDF_Tau_NP_1S = new RooAddPdf("mPDF_Tau_NP_1S","",RooArgSet(*mPDF_Tau_NP_1S_1,*mPDF_Tau_NP_1S_2,*mPDF_Tau_NP_1S_0),RooArgSet(f1,f2,f3));
	//mPDF_Tau_NP_1S = new RooDecay("mPDF_Tau_NP_1S","", *tau, Life_NP_1S, *mPDF_Tau_Reso, RooDecay::SingleSided);

	////////////////////////////////////////////////////////
	/// TIME BKG PDF
	////////////////////////////////////////////////////////
	RooRealVar Life_NP_B1   ("Life_NP_B1",    "", 1.2, 0.08, 3.0);
	RooRealVar Life_NP_B11   ("Life_NP_B11",    "", 1.2, 0.08, 6.0);
	RooRealVar Life_NP_MisID("Life_NP_MisID", "", 0.3, 0.1,  3.0);
	mPDF_Tau_NP_B1 = new RooDecay("t_bkg_decaySS", "", *tau, Life_NP_B1,    *mPDF_Tau_Reso, RooDecay::SingleSided);
	mPDF_Tau_NP_B11 = new RooDecay("t_bkg1_decaySS", "", *tau, Life_NP_B11,    *mPDF_Tau_Reso, RooDecay::SingleSided);
	mPDF_Tau_NP_B2 = new RooDecay("t_bkg_decayDS", "", *tau, Life_NP_MisID, *mPDF_Tau_Reso, RooDecay::DoubleSided);
	//RooGenericPdf mPDF_Tau_Double_Exp("mPDF_Tau_Double_Exp","(@0<0) ? exp(-abs(@0/Life_NP_MisID)) : 0",RooArgSet(*tau,Life_NP_MisID));
	//mPDF_Tau_NP_B2 = new RooFFTConvPdf("t_bkg_decayDS","",*tau,*mPDF_Tau_Reso,mPDF_Tau_Double_Exp);

	//mPDF_Tau_NP_B2 = new RooDecay("t_bkg_decayDS", "", *tau, Life_NP_MisID, *mPDF_Tau_Reso, RooDecay::DoubleSided);

	////////////////////////////////////////////////////////
	/// Invariance Mass Signal for Jpsi and Psi(2S)
	////////////////////////////////////////////////////////
	RooRealVar Mass_Mean_1S("Mass_Mean_1S","",3.096,3.07,3.12);

	RooRealVar Mass_Sigma_1S_G("Mass_Sigma_1S_G","",0.035,0.02,0.1);
	//RooRealVar Mass_CB_SigmaScale("Mass_CB_SigmaScale","",1.70,1.0,2.5);
	RooRealVar Mass_CB_SigmaScale("Mass_CB_SigmaScale","",1.70,1.0,2.5);
	RooFormulaVar Mass_Sigma_1S_CB("Mass_Sigma_1S_CB","Mass_CB_SigmaScale*Mass_Sigma_1S_G",RooArgSet(Mass_CB_SigmaScale,Mass_Sigma_1S_G));
	RooRealVar Mass_CBn("Mass_CBn","",2.0,0.2,3.0);
	Mass_CBn.setConstant(kTRUE);
	RooRealVar Mass_CBa("Mass_CBa","",1.5,1.0,3.0);
	Mass_CBa.setConstant(kTRUE);
	Mass_CB_SigmaScale.setConstant(kTRUE);

	RooRealVar Mass_GaussFrac("Mass_GaussFrac","",0.75,0.5,1.0);
	Mass_GaussFrac.setConstant(kTRUE);

	RooCBShape PDF_Mass_Signal_1S_CB("PDF_Mass_Signal_1S_CB","1S Signal CB", *m, Mass_Mean_1S, Mass_Sigma_1S_CB, Mass_CBa, Mass_CBn);

	RooGaussian PDF_Mass_Signal_1S_G("PDF_Mass_Signal_1S_G","1S Signal G", *m, Mass_Mean_1S, Mass_Sigma_1S_G);

	mPDF_Mass_Signal_1S = new RooAddPdf("PDF_Mass_Signal_1S","",RooArgSet(PDF_Mass_Signal_1S_G,PDF_Mass_Signal_1S_CB),Mass_GaussFrac);
	////////////////////////////////////////////////////////
	/// Mass Bkgd
	////////////////////////////////////////////////////////
	RooRealVar Bkgd_Exp_Prompt("Bkgd_Exp_Prompt","",0,-6.,6.);
	RooRealVar Bkgd_Exp_MisID( "Bkgd_Exp_MisID", "",0,-5.,5.);
	RooRealVar Bkgd_Exp_MisID1( "Bkgd_Exp_MisID1", "",0,-6.,6.);
	//RooRealVar Bkgd_Exp_NonPrompt("Bkgd_Exp_NonPrompt","",-0.5,-10,1);
	RooRealVar Bkgd_Mean( "Bkgd_Mean","",2.6*0.95,1.8,2.6);
	RooRealVar Bkgd_Sigma("Bkgd_Sigma","",0.7,0.5,2.0);
	Bkgd_Sigma.setConstant(kTRUE);
	Bkgd_Mean.setConstant(kTRUE);

	RooRealVar Bkgd_Pow1("Bkgd_Pow1","",3.5,1,11);
	//Bkgd_Pow1.setConstant(kTRUE);
	RooRealVar Bkgd_Const("Bkgd_Const","",0,0,300);
	//Bkgd_Const.setConstant(kTRUE);
	RooRealVar Bkgd_Par("Bkgd_Par","",5,3.51,7);
	//Bkgd_Par.setConstant(kTRUE);
	RooRealVar Bkgd_Exp_NonPrompt("Bkgd_Exp_NonPrompt","",0,-10,10);
	//Bkgd_Exp_NonPrompt.setConstant(kTRUE);
	RooRealVar Bkgd_Pow2("Bkgd_Pow2","",5,1,14);
	//Bkgd_Pow2.setConstant(kTRUE);
	RooRealVar NP_Vato_Frac("NP_Vato_Frac","frac between Vato and Exp",1,0,1);
	//NP_Vato_Frac.setConstant(kTRUE);

	mPDF_Mass_Bkgd_Prompt = new RooExponential("PDF_Mass_Bkgd_Prompt","Bkgd.", *m, Bkgd_Exp_Prompt);
	//mPDF_Mass_Bkgd_Prompt = new RooPolynomial("PDF_Mass_Bkgd_Prompt","Bkgd.", *m);
	mPDF_Mass_Bkgd_MisID  = new RooExponential("PDF_Mass_Bkgd_MisID", "Bkgd.", *m, Bkgd_Exp_MisID);
	mPDF_Mass_Bkgd_MisID1  = new RooExponential("PDF_Mass_Bkgd_MisID1", "Bkgd.", *m, Bkgd_Exp_MisID1);
	//mPDF_Mass_Bkgd_NonPrompt = new RooGaussian("PDF_Mass_Bkgd_NonPrompt","Bkgd.",*m,Bkgd_Mean,Bkgd_Sigma);
	//mPDF_Mass_Bkgd_NonPrompt  = new RooExponential("PDF_Mass_Bkgd_NonPrompt", "Bkgd.", *m, Bkgd_Exp_NonPrompt);
	//mPDF_Mass_Bkgd_NonPrompt = new RooGenericPdf("PDF_Mass_Bkgd_NonPrompt","","Bkgd_Const*exp(-Bkgd_Exp_NonPrompt*(m-4.2))+(0.02*pow(m,Bkgd_Pow1)*pow((Bkgd_Par-m),Bkgd_Pow2))",RooArgSet(*m,Bkgd_Pow1,Bkgd_Const,Bkgd_Par,Bkgd_Exp_NonPrompt,Bkgd_Pow2));
	//RooAbsPdf* mPDF_Mass_Bkgd_NonPrompt_Vato = new RooGenericPdf("PDF_Mass_Bkgd_NonPrompt_Vato","","(pow(m,Bkgd_Pow1)*pow((Bkgd_Par-m),Bkgd_Pow2))",RooArgSet(*m,Bkgd_Pow1,Bkgd_Par,Bkgd_Pow2));
	///*
	RooRealVar Bkgd_m_exp_c("Bkgd_m_exp_c","",0.1,-1,1);
	RooRealVar Bkgd_m_cosh_c("Bkgd_m_cosh_c","",0,0,1);
	RooRealVar Bkgd_m_cosh_c1("Bkgd_m_cosh_c1","",1,0,3);
	RooRealVar Bkgd_m_exp1_c("Bkgd_m_exp1_c","",-0.09,-0.2,1);
	RooRealVar Bkgd_m_exp1_c1("Bkgd_m_exp1_c1","",3.1,3.096,3.2);
	RooAbsPdf* mPDF_Mass_Bkgd_NonPrompt_Vato = new RooGenericPdf("PDF_Mass_Bkgd_NonPrompt_Vato","","Bkgd_m_exp_c*exp(-m)+Bkgd_m_cosh_c/cosh(Bkgd_m_cosh_c1*(m-3.096))+exp(Bkgd_m_exp1_c*(m-Bkgd_m_exp1_c1))",RooArgSet(*m,Bkgd_m_exp_c,Bkgd_m_cosh_c,Bkgd_m_cosh_c1,Bkgd_m_exp1_c,Bkgd_m_exp1_c1));
	//*/
	//RooFormulaVar A("Shift_NP_M_Bkg","m-3.5",RooArgSet(*m));
	RooAbsPdf* mPDF_Mass_Bkgd_NonPrompt_Exp = new RooExponential("PDF_Mass_Bkgd_NonPrompt_Exp", "Bkgd. ", *m, Bkgd_Exp_NonPrompt);
	//mPDF_Mass_Bkgd_NonPrompt = new RooAddPdf("mPDF_Mass_Bkgd_NonPrompt","",RooArgSet(*mPDF_Mass_Bkgd_NonPrompt_Vato,*mPDF_Mass_Bkgd_NonPrompt_Exp), RooArgSet(NP_Vato_Frac));
	//mPDF_Mass_Bkgd_NonPrompt = mPDF_Mass_Bkgd_NonPrompt_Vato; //Only Vato
	mPDF_Mass_Bkgd_NonPrompt = mPDF_Mass_Bkgd_NonPrompt_Exp;
	//*************TEST******************//
	RooRealVar Bkgd_m_argpar("Bkgd_m_argpar","argus shape parameter",-20.0,-100.,-1.);
	RooRealVar Bkgd_m_m0("Bkgd_m_m0","argus m0",3.0,2.7,3.5);
	RooArgusBG argus("argus","Argus PDF",*m,Bkgd_m_m0,Bkgd_m_argpar);

	RooRealVar a0("a0","a0",-0.1,-1,1) ;
	RooRealVar a1("a1","a1",0.0,-1,1) ;
	RooRealVar a2("a2","a2",0.0,-1,1) ;
	RooChebychev* chebyshev = new RooChebychev("chebyshev","chebyshev PDF",*m,RooArgSet(a0,a1,a2)) ;
	//mPDF_Mass_Bkgd_NonPrompt = new RooAddPdf("mPDF_Mass_Bkgd_NonPrompt","",RooArgSet(argus,chebyshev), RooArgSet(NP_Vato_Frac));
	//mPDF_Mass_Bkgd_NonPrompt = (RooAbsPdf*)chebyshev;
	//**********************************//

	////////////////////////////////////////////////////////
	/// Build Complete Model
	////////////////////////////////////////////////////////
	RooRealVar f_NP_1S("f_NP_1S","1S non-prompt fraction", 0.4, 0.0, 1.0);
	RooRealVar f_NP_B_MisID("f_NP_B_MisID","Bkgd. NP Fraction",0.2,0.0,1.0); //0.2
	RooRealVar f_NP_B_MisID1("f_NP_B_MisID1","Bkgd. NP Fraction 1",0.04,0.0,1.0); //0.2
	RooRealVar f_NP_B("f_NP_B","Bkgd. NP Fraction",0.9,0.0,1.0); //0.2


	RooAbsPdf* PDF_P_1S  = new RooProdPdf("PDF_P_1S", "", RooArgSet(*mPDF_Mass_Signal_1S,*mPDF_Tau_Reso) );
	RooAbsPdf* PDF_NP_1S = new RooProdPdf("PDF_NP_1S","", RooArgSet(*mPDF_Mass_Signal_1S,*mPDF_Tau_NP_1S));
	RooAbsPdf* PDF_1S = new RooAddPdf("PDF_1S", "",RooArgList(*PDF_NP_1S,*PDF_P_1S),f_NP_1S);

	RooAbsPdf* PDF_P_B1  = new RooProdPdf("PDF_P_B1", "", RooArgSet(*mPDF_Mass_Bkgd_Prompt,   *mPDF_Tau_Reso) );      //bkgd1
	RooAbsPdf* PDF_NP_B1 = new RooProdPdf("PDF_NP_B1","", RooArgSet(*mPDF_Mass_Bkgd_NonPrompt,*mPDF_Tau_NP_B1));      //bkgd4
	RooAbsPdf* PDF_NP_B2 = new RooProdPdf("PDF_NP_B2","", RooArgSet(*mPDF_Mass_Bkgd_MisID,    *mPDF_Tau_NP_B2));      //bkgd3
	RooAbsPdf* PDF_NP_B21 = new RooProdPdf("PDF_NP_B21","", RooArgSet(*mPDF_Mass_Bkgd_MisID1,    *mPDF_Tau_NP_B11));  //bkgd2

	//RooAbsPdf* PDF_NP_Bs  = new RooAddPdf("PDF_NP_Bs", "",RooArgList(*PDF_NP_B2,*PDF_NP_B1),RooArgList(f_NP_B_MisID));
	//RooAbsPdf* PDF_NP_B_MisID  = new RooAddPdf("PDF_NP_B_MisID", "",RooArgList(*PDF_NP_B21,*PDF_NP_B2),RooArgList(f_NP_B_MisID1));
	//RooAbsPdf* PDF_NP_B  = new RooAddPdf("PDF_NP_B", "",RooArgList(*PDF_NP_B_MisID,*PDF_NP_B1),RooArgList(f_NP_B_MisID));

	RooAbsPdf* PDF_NP_B  = new RooAddPdf("PDF_NP_B", "",RooArgList(*PDF_NP_B2,*PDF_NP_B1),RooArgList(f_NP_B_MisID));
	//RooAbsPdf* PDF_NP_B  = (RooAbsPdf*)PDF_NP_B2->Clone();
	RooAbsPdf* PDF_B     = new RooAddPdf("PDF_B", "", RooArgSet(*PDF_NP_B,*PDF_P_B1),RooArgSet(f_NP_B));

	//RooRealVar N_1S("N_1S","",0.30*data->sumEntries(), 0.10*data->sumEntries(), 0.80*data->sumEntries());
	RooRealVar N_1S("N_1S","",0.30*data->sumEntries(), 0.10*data->sumEntries(), 1*data->sumEntries());
	//RooRealVar N_B ("N_B", "",0.60*data->sumEntries(), 0.05*data->sumEntries(), 0.90*data->sumEntries());
	RooRealVar N_B ("N_B", "",0.60*data->sumEntries(), 0*data->sumEntries(), 0.90*data->sumEntries());

	// Extended PDF
	RooAbsPdf* ePDF_1S = new RooExtendPdf("ePDF_1S","",*PDF_1S,N_1S);
	RooAbsPdf* ePDF_B  = new RooExtendPdf("ePDF_B", "",*PDF_B, N_B );

	// Derived Yields
	RooFormulaVar N_1S_P ("N_1S_P", "N_1S*(1.0-f_NP_1S)",RooArgSet(N_1S,f_NP_1S));
	RooFormulaVar N_1S_NP("N_1S_NP","N_1S*f_NP_1S",      RooArgSet(N_1S,f_NP_1S));

	// Total Extended PDF
	RooAbsPdf* model = new RooAddPdf("ePDF_Total","",RooArgSet(*ePDF_1S,*ePDF_B));
	/*
	   RooBinning* MassBinning = new RooBinning(2.6, 4.2,"MassBinning");
	   MassBinning->addUniform(80, 2.6, 4.2);
	   m->setBinning(*MassBinning);

	   RooBinning* TauBinning = new RooBinning(-1.5, 15,"TauBinning");
	   TauBinning->addUniform(400, -1.5, 15);
	   tau->setBinning(*TauBinning);
	   */
	//tau->setBins(40);
	RooDataHist* BinnedDataSample = data->binnedClone("BinnedDataSample","");
	data->Print();
	BinnedDataSample->Print();

	std::cout << "(Post Cut) DataSample numEntries = " << data->numEntries() << " " << data->sumEntries() << std::endl;
	std::cout << "(Post Cut) BinnedDataSample numEntries = " << BinnedDataSample->numEntries() << " " << BinnedDataSample->sumEntries() << std::endl;

	/*TIterator* iter = params->createIterator() ;
	  while((parg=(RooRealVar*)iter->Next())) {
	  cout<<parg->getValV()<<endl;
	  }*/

	//RooDataSet* BinnedDataSample = (RooDataSet*)data->Clone();

	//////////////////////////////////////////
	/// Mass 1D
	//////////////////////////////////////////
	RooAbsPdf* M1D_PDF_1S = mPDF_Mass_Signal_1S;
	RooAbsPdf* M1D_PDF_B  = mPDF_Mass_Bkgd_Prompt;

	// Extended PDF
	RooAbsPdf* M1D_ePDF_1S = new RooExtendPdf("M1D_ePDF_1S","",*M1D_PDF_1S,N_1S);
	RooAbsPdf* M1D_ePDF_B  = new RooExtendPdf("M1D_ePDF_B", "",*M1D_PDF_B, N_B );

	// Total Extended PDF
	RooAbsPdf* M1D_model = new RooAddPdf("M1D_ePDF_Total","",RooArgSet(*M1D_ePDF_1S,*M1D_ePDF_B));

	//-----------------------------------------

	//Extract params
	RooArgSet* params = model->getParameters(*data);
	TIterator *iterat= savedParams->createIterator();
	RooAbsArg *next = 0;
	if( 0 != (next= (RooAbsArg*)iterat->Next()) ){
		*params = *savedParams;
	}
	params->Print("v");
	//Bkgd_Exp_MisID.Print("v");
	////////////////////////////////////////////////////////
	/// Fit data
	////////////////////////////////////////////////////////
	int cores = atoi(gSystem->GetFromPipe("nproc"));

	if(useInput || drawOnly){
		params->readFromFile(inputF->Data());
		//tmp
		/*
		   int cnt = 0;
		   TIterator* iter = params->createIterator() ;
		   RooRealVar *parg = 0;
		   while((parg=(RooRealVar*)iter->Next())) {
		   if(!(cnt==6 || cnt==30))
		   parg->setConstant(kTRUE);
		   cnt++;
		   }
		   */
		if(drawOnly){
			TIterator* iter = params->createIterator() ;
			RooRealVar *parg = 0;
			while((parg=(RooRealVar*)iter->Next())) {
				parg->setConstant(kTRUE);
			}
		}

		params->Print("v");
	}else{
		RooDataSet* mass1D = (RooDataSet*)BinnedDataSample->reduce(SelectVars(RooArgSet(*m)));
		{
			RooAbsReal* nll = M1D_model->createNLL(*mass1D,NumCPU(cores),Offset(0));

			RooMinimizer min(*nll);
			min.setStrategy(2);

			min.optimizeConst(kTRUE);
			min.minimize("Minuit2","Migrad");
			min.hesse();
			min.save()->Print("v");
		}
		double upper = Mass_Mean_1S.getValV()-6*Mass_Sigma_1S_G.getValV();
		double lower = Mass_Mean_1S.getValV()+6*Mass_Sigma_1S_G.getValV();

		//Mass_Mean_1S.setConstant(kTRUE);
		//Mass_Sigma_1S_G.setConstant(kTRUE);

		m->setRange("SB1",2.6,upper);
		m->setRange("SB2",lower,3.5);

		RooAbsReal* nll = PDF_B->createNLL(*BinnedDataSample,Range("SB1,SB2"),NumCPU(cores),Offset(0));

		RooMinimizer min(*nll);
		min.setStrategy(2);

		//min.optimizeConst(kTRUE);
		min.minimize("Minuit2","Migrad");
		min.hesse();
		min.save()->Print("v");
		//PDF_B->fitTo(*BinnedDataSample, Range("SB1,SB2,SB3"), Save(),NumCPU(8));
		//PDF_B->fitTo(*data, Range("SB1,SB2,SB3"), Save(), NumCPU(cores));
		//PDF_B->fitTo(*BinnedDataSample, Range("SB1,SB2"), Save(), NumCPU(cores));
		//m_result = model->fitTo(*BinnedDataSample,Save(kTRUE),SumW2Error(kFALSE),Offset(kTRUE));
		//f_NP_B.setConstant(kTRUE);
		//f_NP_B_MisID.setConstant(kTRUE);
		////Mass_Mean_1S.setConstant(kTRUE);
		//Mass_Sigma_1S_G.setConstant(kTRUE);
		//Bkgd_Exp_MisID.setConstant(kTRUE);
		//Bkgd_Exp_MisID1.setConstant(kTRUE);
		//Bkgd_Exp_NonPrompt.setConstant(kTRUE);
		//Bkgd_Exp_Prompt.setConstant(kTRUE);
		//NP_Vato_Frac.setConstant(kTRUE);
		//Life_NP_1S_Short.setConstant(kTRUE);
		//Life_NP_1S      .setConstant(kTRUE);
		//Life_NP_2S      .setConstant(kTRUE);
		//Life_NP_B1      .setConstant(kTRUE);
		//Life_NP_B11     .setConstant(kTRUE);
		//Life_NP_MisID   .setConstant(kTRUE);

		//f_NP_B_MisID.setRange(f_NP_B_MisID.getValV()-0.6*fabs(f_NP_B_MisID.getValV()),f_NP_B_MisID.getValV()+0.6*fabs(f_NP_B_MisID.getValV()));
		//f_NP_B.setRange(f_NP_B.getValV()-0.6*fabs(f_NP_B.getValV()),f_NP_B.getValV()+0.6*fabs(f_NP_B.getValV()));
		//Bkgd_Exp_MisID.setRange(Bkgd_Exp_MisID.getValV()-0.5*fabs(Bkgd_Exp_MisID.getValV()),Bkgd_Exp_MisID.getValV()+0.5*fabs(Bkgd_Exp_MisID.getValV()));
		//Bkgd_Exp_MisID.Print();
		//Bkgd_Exp_NonPrompt.setRange(Bkgd_Exp_NonPrompt.getValV()-0.5*fabs(Bkgd_Exp_NonPrompt.getValV()),Bkgd_Exp_NonPrompt.getValV()+0.5*fabs(Bkgd_Exp_NonPrompt.getValV()));
		//Bkgd_Exp_Prompt.setRange(Bkgd_Exp_Prompt.getValV()-0.7*fabs(Bkgd_Exp_Prompt.getValV()),Bkgd_Exp_Prompt.getValV()+0.7*fabs(Bkgd_Exp_Prompt.getValV()));
	}

	//Number of parameters
	TIterator* iter = params->createIterator() ;
	RooRealVar *parg = 0;
	while((parg=(RooRealVar*)iter->Next())) {
		if(!parg->getAttribute("Constant"))
			nparams++;
	}

	RooAbsReal* nll = model->createNLL(*BinnedDataSample,NumCPU(cores),Offset(0));


	RooMinimizer min(*nll);

	//min.optimizeConst(kTRUE);
	//min.setOffsetting(kFALSE);
	min.setStrategy(2);

	min.minimize("Minuit2","Migrad");
	min.hesse();
	//min.minos();
	//min.minos(*params);
	m_result = min.save();

	//m_result = model->fitTo(*BinnedDataSample,NumCPU(8),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE));
	//m_result = model->fitTo(*BinnedDataSample,NumCPU(8),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE),Minimizer("Minuit2","migrad"));
	//m_result = model->fitTo(*data,NumCPU(cores),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE));
	//m_result = model->fitTo(*BinnedDataSample,NumCPU(cores),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE),Minimizer("Minuit2","migrad"));
	//m_result = model->fitTo(*BinnedDataSample,NumCPU(cores),Optimize(0),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE));
	//m_result = model->chi2FitTo(*BinnedDataSample,NumCPU(cores),Strategy(2),Save(),SumW2Error(kTRUE),Timer(kTRUE));
	////////////////////////////////////////////////////////
	/// Set RooPlots
	////////////////////////////////////////////////////////

	m_mframe = m->frame(Bins(64), Title("Dimuon mass"));
	data ->plotOn(m_mframe,Name("m_data"), Invisible());
	model->plotOn(m_mframe,Name("m_model"), Invisible());
	model->plotOn(m_mframe,Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)), FillColor(kBlue+2),LineColor(kBlue+2),DrawOption("F"),FillStyle(3345),Precision(1e-5));
	model->plotOn(m_mframe,Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kWhite),LineColor(kRed+2),DrawOption("CF"),Precision(1e-5));
	model->plotOn(m_mframe,Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kRed+2),LineColor(kRed+2),DrawOption("F"),FillStyle(3344),Precision(1e-5));
	model->plotOn(m_mframe,Components(RooArgSet(*PDF_B,*PDF_NP_1S)),LineColor(kRed+2),Precision(1e-5));
	model->plotOn(m_mframe,Components(*PDF_B),      FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
	model->plotOn(m_mframe,Components(*PDF_B),      LineColor(kGray+2),Precision(1e-5));
	//model->plotOn(m_mframe,Components(argus),LineColor(kRed),LineStyle(kDashed)) ;
	//model->plotOn(m_mframe,Components(chebyshev),LineStyle(kDashed)) ;
	data ->plotOn(m_mframe,MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
	model->plotOn(m_mframe, LineColor(1),Precision(1e-5));
	data->statOn(m_mframe,Layout(0.55,0.95,0.95));
	model->paramOn(m_mframe,Layout(0.47));
	m_mframe->getAttText()->SetTextSize(0.025);
	double m_ReducedChi2_m = m_mframe->chiSquare("m_model","m_data",nparams);
	TText* txt_chi2_m = new TText(0.2,0.7,Form("Chi2/ndof : %4f", m_ReducedChi2_m)) ;
	txt_chi2_m->SetNDC();
	txt_chi2_m->SetTextSize(0.04) ;
	txt_chi2_m->SetTextColor(kRed) ;
	m_mframe->addObject(txt_chi2_m) ;

	RooHist* m_hpull = m_mframe->pullHist();
	m_pullframe = m->frame(Bins(64), Title("Dimuon mass pull"));
	m_pullframe->addPlotable((RooPlotable *)m_hpull,"P");

	//t_mframe = tau->frame(Bins(1650), Title("proper time"));
	t_mframe = tau->frame(Bins(330), Title("proper time"));
	t_mframe->SetAxisRange(-4,11);
	data ->plotOn(t_mframe,Name("t_data"),Invisible());
	model->plotOn(t_mframe,Name("t_model"),Invisible());
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)), FillColor(kGray),LineColor(kGray+2),DrawOption("CF"),Precision(1e-5));
	// Prompt Signal
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)),FillColor(kWhite),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)),FillColor(kBlue+2),LineColor(kBlue+2),DrawOption("F"),FillStyle(3345),Precision(1e-5));
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)),LineColor(kBlue+2),Precision(1e-5));
	// Non-Prompt Signal
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_NP_1S)),FillColor(kWhite),LineColor(kRed+2),DrawOption("CF"),Precision(1e-5));
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_NP_1S)),FillColor(kRed+2),LineColor(kRed+2),DrawOption("F"),FillStyle(3344),Precision(1e-5));
	model->plotOn(t_mframe,Components(RooArgSet(*PDF_NP_1S)),LineColor(kRed+2),Precision(1e-5));
	//
	//model->plotOn(t_mframe,Components(RooArgSet(mPDF_Tau_NP_1S_Long)),FillColor(kWhite),LineColor(kOrange+10),DrawOption("CF"),Precision(1e-5));
	//model->plotOn(t_mframe,Components(RooArgSet(mPDF_Tau_NP_1S_Long)),FillColor(kOrange+10),LineColor(kOrange+10),DrawOption("F"),FillStyle(3344),Precision(1e-5));
	//model->plotOn(t_mframe,Components(RooArgSet(mPDF_Tau_NP_1S_Long)),LineColor(kOrange+10),Precision(1e-5));
	// Data and total fit
	data ->plotOn(t_mframe,MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
	model->plotOn(t_mframe,LineColor(1),Precision(1e-5));
	//model->plotOn(t_mframe,LineColor(1),Precision(1e-5),ProjWData(*m,*BinnedDataSample));
	data->statOn(t_mframe,Layout(0.55,0.95,0.95));
	model->paramOn(t_mframe,Layout(0.47,0.95));
	t_mframe->getAttText()->SetTextSize(0.025);
	double m_ReducedChi2_tau = t_mframe->chiSquare("t_model","t_data",nparams);
	TText* txt_chi2_tau = new TText(0.2,0.7,Form("Chi2/ndof : %4f", m_ReducedChi2_tau)) ;
	txt_chi2_tau->SetNDC();
	txt_chi2_tau->SetTextSize(0.04) ;
	txt_chi2_tau->SetTextColor(kRed) ;
	t_mframe->addObject(txt_chi2_tau) ;

	//t_mframe_pull = tau->frame(Bins(1650), Title("proper time pull"));
	RooHist* t_hpull = t_mframe->pullHist(0,0,kTRUE);
	t_pullframe = tau->frame(Bins(800), Title("proper time pull"));
	t_pullframe->addPlotable((RooPlotable *)t_hpull,"P");

	//2D plots
	/*
	   double low=0, high=0;
	   data->getRange(*tau,low,high);
	   cout<<low<<" "<<high<<endl;
	   RooRealVar* xvar = (RooRealVar*) data->get()->find("tau") ;
	   cout<<xvar->getMin()<<" "<<xvar->getMax()<<endl;;
	   */
	//hh_data = data->createHistogram("m,tau",64,600);
	hh_data = data->createHistogram("hh_data",*m,Binning(72,2.6,3.5),YVar(*tau,Binning(800,-1.0,15.)));
	hh_pdf = model->createHistogram("m,tau",72,800);
	//hf_pdf = model->asTF(RooArgList(*m,*tau),params);

	m_ReducedChi2 = m_mframe->chiSquare("m_model","m_data",nparams);

	////////////////////////////////////////////////////////
	/// Save Params for next bin
	///////////////////////////////////////////////////////
	RooArgSet* tmp = (RooArgSet*)params->snapshot(kTRUE);
	//savedParams = (RooArgSet*)tmp->clone("saved");
	savedParams->addOwned(*tmp);
	*savedParams = *tmp;
	std::cout<<tmp<<" "<<savedParams<<" "<<*tmp<<" "<<*savedParams<<std::endl;
	tmp->Print("v");
	//std::ofstream outfile(outF->Data(),std::ios_base::app);
	//outfile<<"y: "<<theHelper.y_index<<" pt: "<<theHelper.pt_index<<endl;
	if(!drawOnly){
		//std::cout.precision(15);
		//savedParams->writeToFile(outF->Data());
		std::ofstream os(outF->Data(),std::ios_base::app);
		os.precision(15);
		savedParams->writeToStream(os,0);
		m_result->printValue(os);
		os.close();
		//outfile.close();
	}
	if(m_ReducedChi2_m > 7 || m_ReducedChi2_tau > 7)
		savedParams->removeAll();
	savedParams->Print("v");



	if(!drawOnly){
		m_val.Inclusive1S 	  = N_1S.getVal();
		m_val.Inclusive1S_Err = N_1S.getPropagatedError(*m_result);
		m_val.Prompt1S = N_1S_P.getVal();
		m_val.Prompt1S_Err = N_1S_P.getPropagatedError(*m_result);
		m_val.NonPrompt1S = N_1S_NP.getVal();
		m_val.NonPrompt1S_Err = N_1S_NP.getPropagatedError(*m_result);
		m_val.NPFraction1S       = f_NP_1S.getVal();
		m_val.NPFraction1S_Err   = f_NP_1S.getPropagatedError(*m_result);

		m_val.MassSigma1S = TMath::Sqrt(Mass_Sigma_1S_CB.getVal()*Mass_Sigma_1S_CB.getVal());
		m_val.TauSigma1   = Tau_Sigma1.getVal();
		m_val.TauSigma2   = Tau_Sigma2.getVal();

		m_tau_sigma = Tau_Sigma1.getVal();
		m_life_1s = Life_NP_1S.getVal();

		m_result->Print("v");
	}

	//m_frac.fsignal      = fsignal.getVal();
	//m_frac.fsigPrompt   = fsigPrompt.getVal();
	//m_frac.fracPPsi2S   = fPPsi2S.getVal();
	//m_frac.fracNPPsi2S  = fNPPsi2S.getVal();
	//m_frac.fbkg1        = f_bkg_b1.getVal();
	//m_frac.fbkg2        = f_bkg_b2.getVal();
	//m_fsigma = fSigma.getVal();
	////////////////////////////////////////////////////////
	/// Set Additional RooPlots
	////////////////////////////////////////////////////////
	if (m_extraPlots) {
		//m_mframe_RangePsi   = m->frame(Bins(75), Title("Dimuon mass"));
		m_mframe_RangeP     = m->frame(Bins(64), Title("Dimuon mass P"));
		m_mframe_RangeNP    = m->frame(Bins(64), Title("Dimuon mass NP"));
		m_mframe_RangeNP1    = m->frame(Bins(64), Title("Dimuon mass NP1"));
		m_mframe_RangeMisID = m->frame(Bins(64), Title("Dimuon mass MisID"));
		t_mframe_RangePreJpsi  = tau->frame(Bins(800), Title("proper time PreJpsi"));
		t_mframe_RangeJpsi  = tau->frame(Bins(800), Title("proper time Jpsi"));
		t_mframe_RangePrePsi   = tau->frame(Bins(800), Title("proper time PrePsi"));

		/// Jpsi lifetime fit projection
		m->  setRange("MassRangePreJpsi",2.6,2.85);
		RooDataSet* data_RangePreJpsi = (RooDataSet*)data->reduce(Cut("m>2.6 && m<2.85"));
		data_RangePreJpsi->plotOn(t_mframe_RangePreJpsi, Invisible());
		model       ->plotOn(t_mframe_RangePreJpsi, NormRange("MassRangePreJpsi"),ProjectionRange("MassRangePreJpsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangePreJpsi, NormRange("MassRangePreJpsi"),ProjectionRange("MassRangePreJpsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangeJpsi, NormRange("MassRangePreJpsi"),ProjectionRange("MassRangePreJpsi"),Components(RooArgSet(*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangePreJpsi->plotOn(t_mframe_RangePreJpsi, MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
		model         ->plotOn(t_mframe_RangePreJpsi, NormRange("MassRangePreJpsi"),ProjectionRange("MassRangePreJpsi"), LineColor(kRed-3),Precision(1e-5));
		data_RangePreJpsi->statOn(t_mframe_RangePreJpsi,Layout(0.55,0.95,0.95));

		m->  setRange("MassRangeJpsi",2.85,3.35);
		//tau->setRange("MassRangeJpsi",-5,15);
		RooDataSet* data_RangeJpsi = (RooDataSet*)data->reduce(Cut("m>2.85 && m<3.35"));
		data_RangeJpsi->plotOn(t_mframe_RangeJpsi, Invisible());
		model	      ->plotOn(t_mframe_RangeJpsi, ProjectionRange("MassRangeJpsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model	      ->plotOn(t_mframe_RangeJpsi, ProjectionRange("MassRangeJpsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model	      ->plotOn(t_mframe_RangeJpsi,ProjectionRange("MassRangeJpsi"),Components(RooArgSet(*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeJpsi->plotOn(t_mframe_RangeJpsi, MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
		//model         ->plotOn(t_mframe_RangeJpsi, NormRange("MassRangeJpsi"),ProjectionRange("MassRangeJpsi"), LineColor(kRed-3),Precision(1e-5));
		model         ->plotOn(t_mframe_RangeJpsi, ProjectionRange("MassRangeJpsi"), LineColor(kRed-3),Precision(1e-5));
		data_RangeJpsi->statOn(t_mframe_RangeJpsi,Layout(0.55,0.95,0.95));

		m->  setRange("MassRangePrePsi",3.35,3.55);
		RooDataSet* data_RangePrePsi = (RooDataSet*)data->reduce(Cut("m>3.35 && m<3.55"));
		data_RangePrePsi->plotOn(t_mframe_RangePrePsi, Invisible());
		model       ->plotOn(t_mframe_RangePrePsi, NormRange("MassRangePrePsi"),ProjectionRange("MassRangePrePsi"),Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangePrePsi, NormRange("MassRangePrePsi"),ProjectionRange("MassRangePrePsi"),Components(RooArgSet(*PDF_P_1S,*PDF_NP_1S)), FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(t_mframe_RangePsi, NormRange("MassRangePrePsi"),ProjectionRange("MassRangePrePsi"),Components(RooArgSet(*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangePrePsi->plotOn(t_mframe_RangePrePsi, MarkerSize(1.0),XErrorSize(0),DataError(RooAbsData::SumW2));
		model         ->plotOn(t_mframe_RangePrePsi, NormRange("MassRangePrePsi"),ProjectionRange("MassRangePrePsi"), LineColor(kRed-3),Precision(1e-5));
		data_RangePrePsi->statOn(t_mframe_RangePrePsi,Layout(0.55,0.95,0.95));

		/// Prompt evt mass projection
		/// mis-ID evt mass projection
		//m->  setRange("TauRangeMisID", 2.6, 4.2);
		tau->setRange("TauRangeMisID",-1.0,-0.6);
		RooDataSet* data_RangeMisID = (RooDataSet*)data->reduce(Cut("tau>-1.0 && tau<-0.6"));
		data_RangeMisID->plotOn(m_mframe_RangeMisID, Invisible());
		model          ->plotOn(m_mframe_RangeMisID, NormRange("TauRangeMisID"), ProjectionRange("TauRangeMisID"), Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)),FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model          ->plotOn(m_mframe_RangeMisID, NormRange("TauRangeMisID"), ProjectionRange("TauRangeMisID"), Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model          ->plotOn(m_mframe_RangeMisID, NormRange("TauRangeMisID"), ProjectionRange("TauRangeMisID"), Components(*PDF_B), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeMisID->plotOn(m_mframe_RangeMisID, MarkerSize(1.0),XErrorSize(0), DataError(RooAbsData::SumW2));
		model          ->plotOn(m_mframe_RangeMisID, NormRange("TauRangeMisID"), ProjectionRange("TauRangeMisID"), LineColor(kRed-3),Precision(1e-5));
		data_RangeMisID->statOn(m_mframe_RangeMisID,Layout(0.55,0.95,0.95));


		//m->  setRange("TauRangeP", 2.6,4.2);
		tau->setRange("TauRangeP",-0.6,0.6);
		RooDataSet* data_RangeP = (RooDataSet*)data->reduce(Cut("tau>-0.6 && tau<0.6"));
		data_RangeP->plotOn(m_mframe_RangeP, Invisible());
		model      ->plotOn(m_mframe_RangeP, NormRange("TauRangeP"), ProjectionRange("TauRangeP"), Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)),FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model      ->plotOn(m_mframe_RangeP, NormRange("TauRangeP"), ProjectionRange("TauRangeP"), Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model      ->plotOn(m_mframe_RangeP, NormRange("TauRangeP"), ProjectionRange("TauRangeP"), Components(*PDF_B), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeP->plotOn(m_mframe_RangeP, MarkerSize(1.0), XErrorSize(0), DataError(RooAbsData::SumW2));
		model      ->plotOn(m_mframe_RangeP, NormRange("TauRangeP"), ProjectionRange("TauRangeP"), LineColor(kRed-3),Precision(1e-5));
		data_RangeP->statOn(m_mframe_RangeP,Layout(0.55,0.95,0.95));

		/// Non-Prompt evt mass projection
		//m  ->setRange("TauRangeNP", 2.6, 4.2);
		tau->setRange("TauRangeNP", 0.6, 3.0);
		RooDataSet* data_RangeNP = (RooDataSet*)data->reduce(Cut("tau>0.6 && tau<3.0"));
		data_RangeNP->plotOn(m_mframe_RangeNP, Invisible());
		model       ->plotOn(m_mframe_RangeNP, NormRange("TauRangeNP"), ProjectionRange("TauRangeNP"), Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)),FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(m_mframe_RangeNP, NormRange("TauRangeNP"), ProjectionRange("TauRangeNP"), Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(m_mframe_RangeNP, NormRange("TauRangeNP"), ProjectionRange("TauRangeNP"), Components(*PDF_B), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeNP->plotOn(m_mframe_RangeNP, MarkerSize(1.0), XErrorSize(0), DataError(RooAbsData::SumW2));
		model       ->plotOn(m_mframe_RangeNP, NormRange("TauRangeNP"), ProjectionRange("TauRangeNP"), LineColor(kRed-3),Precision(1e-5));
		data_RangeNP->statOn(m_mframe_RangeNP,Layout(0.55,0.95,0.95));

		tau->setRange("TauRangeNP1", 3.0, 15.);
		RooDataSet* data_RangeNP1 = (RooDataSet*)data->reduce(Cut("tau>3.0"));
		data_RangeNP1->plotOn(m_mframe_RangeNP1, Invisible());
		model       ->plotOn(m_mframe_RangeNP1, NormRange("TauRangeNP1"), ProjectionRange("TauRangeNP1"), Components(RooArgSet(*PDF_B,*PDF_P_1S,*PDF_NP_1S)),FillColor(kAzure-9),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(m_mframe_RangeNP1, NormRange("TauRangeNP1"), ProjectionRange("TauRangeNP1"), Components(RooArgSet(*PDF_B,*PDF_NP_1S)),FillColor(kAzure-5),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		model       ->plotOn(m_mframe_RangeNP1, NormRange("TauRangeNP1"), ProjectionRange("TauRangeNP1"), Components(*PDF_B), FillColor(kGray),LineColor(kWhite),DrawOption("CF"),Precision(1e-5));
		data_RangeNP1->plotOn(m_mframe_RangeNP1, MarkerSize(1.0), XErrorSize(0), DataError(RooAbsData::SumW2));
		model       ->plotOn(m_mframe_RangeNP1, NormRange("TauRangeNP1"), ProjectionRange("TauRangeNP1"), LineColor(kRed-3),Precision(1e-5));
		data_RangeNP1->statOn(m_mframe_RangeNP1,Layout(0.55,0.95,0.95));

		if(!drawOnly)
			m_result->Print("v");
	}

	if (Tau_Reso1) { delete Tau_Reso1; Tau_Reso1 = 0; }
	if (Tau_Reso2) { delete Tau_Reso2; Tau_Reso2 = 0; }
	if (PDF_P_1S)  { delete PDF_P_1S;  PDF_P_1S = 0;  }
	if (PDF_NP_1S) { delete PDF_NP_1S; PDF_NP_1S = 0; }
	if (PDF_P_B1)  { delete PDF_P_B1;  PDF_P_B1 = 0;  }
	if (PDF_NP_B1) { delete PDF_NP_B1; PDF_NP_B1 = 0; }
	if (PDF_NP_B2) { delete PDF_NP_B2; PDF_NP_B2 = 0; }
	if (PDF_NP_B)  { delete PDF_NP_B;  PDF_NP_B = 0;  }
	if (PDF_1S)    { delete PDF_1S;    PDF_1S = 0;    }
	if (PDF_B)     { delete PDF_B;     PDF_B = 0;     }
	if (ePDF_1S)   { delete ePDF_1S;   ePDF_1S = 0;   }
	if (ePDF_B)    { delete ePDF_B;    ePDF_B = 0;    }
	if (model)     { delete model;     model = 0;     }

	return m_result;
}

