from sympy import *
#init_printing()

x = Symbol('x',real=True)
y = Symbol('y',real=True)
rho = Symbol('rho', real=True )
mu1 = Symbol('mu1',real=True)
mu2 = Symbol('mu2',real=True)
s1 = Symbol('s1', real=True, positive=True)
s2 = Symbol('s2', real=True, positive=True)

rho =0

a = (x-mu1)**2/s1**2
b = (y-mu2)**2/s2**2
c = 2*rho*(x-mu1)*(y-mu2)/(s1*s2)

f = exp(-(a+b-c)/(2*(1-rho**2)))/(2*pi*s1*s2*sqrt(1-rho**2))

i = integrate(f,(x,-oo,oo))
#ans = refine(i,Q.positive(rho+1) & Q.negative(rho-1))

print(f)
