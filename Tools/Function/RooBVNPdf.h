/*****************************************************************************
 * Project: RooFit                                                           *
 *                                                                           *
 * This code was autogenerated by RooClassFactory                            *
 *****************************************************************************/

#ifndef ROOBVNPDF
#define ROOBVNPDF

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooCategoryProxy.h"
#include "RooAbsReal.h"
#include "RooAbsCategory.h"

class RooBVNPdf : public RooAbsPdf {
	public:
		RooBVNPdf() {} ;
		RooBVNPdf(const char *name, const char *title,
				RooAbsReal& _x,
				RooAbsReal& _x_mean,
				RooAbsReal& _x_sigma,
				RooAbsReal& _y,
				RooAbsReal& _y_mean,
				RooAbsReal& _y_sigma,
				RooAbsReal& _rho);
		RooBVNPdf(const RooBVNPdf& other, const char* name=0) ;
		virtual TObject* clone(const char* newname) const { return new RooBVNPdf(*this,newname); }
		inline virtual ~RooBVNPdf() { }

		Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
		Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

	protected:

		RooRealProxy x ;
		RooRealProxy x_mean ;
		RooRealProxy x_sigma ;
		RooRealProxy y ;
		RooRealProxy y_mean ;
		RooRealProxy y_sigma ;
		RooRealProxy rho ;

		Double_t evaluate() const ;

	private:

		ClassDef(RooBVNPdf,1) // Your description goes here...
};

#endif
